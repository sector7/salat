# Learning und Softcomputing, Sommersemester 2015 #

## Teilnehmer Gruppe 4 ##
* Ellen Schwartau, 101493, inf101493@fh-wedel.de
* Malte Jörgens, 101519, inf101519@fh-wedel.de
* Timo Gröger, 101504, inf101504@fh-wedel.de

* * *

## Inhalt dieses Verzeichnisses ##

Datei/Ordner  | Inhalt
------------- | -------------
./doc         | Dokumente (Aufgabenstellung, JavaDoc, Auswertungsdaten, Ausarbeitung zum Projekt)
./src         | vollständig übersetzbarer Quellcode
./test        | JUnit-Tests
./.settings   | Eclipse Formatter Einstellungen
.classpath    | Eclipse Klassenpfade
.project      | Eclipse-Projekt
Makefile      | zum Übersetzen und Ausführen des Projekts
build.xml     | `ant`-Buildfile zum Übersetzen und Ausführen des Projekts

* * *

## Programmvoraussetzungen ##
* Java 8 RE
* Make
* [Apache Ant](http://ant.apache.org)
* JavaDoc (optional, benötigt für `make javadoc`)
* [GnuPlot](www.gnuplot.info) (optional, benötigt für automatische Erzeugung von Graphen)

## make Befehle ##
Bei keiner Parameterangabe wird das Ziel `build` ausgeführt (unten fett). Die `ant`-Befehle entsprechen denjenigen von `make`.

Befehl            | Effekt
----------------- | -----------------
  `all`           | Führt nacheinander die Befehle `test`, `javadoc` und `run` aus
  `javadoc`       | Erstellt das JavaDoc im Verzeichnis doc/javadoc
**`build`**       | Kompiliert alle Quelldateien
  `run`           | Kompiliert ggf. alle Quelldateien und startet das Programm
  `test`          | Kompiliert ggf. alle Quelldateien und startet die JUnit-Tests
  `clean`         | Bereinigen der Kompilate
  `cleanall`      | Bereinigen der Kompilate, des JavaDoc und des Ausgabeverzeichnisses der Auswertung

Für das Target `run` ist außerdem möglich, eine Variable zu setzen, die die durchzuführenden Optimierungen vorgibt: `make run arg="lm"` (bzw. `ant run -Darg="lm"`) führt beispielsweise eine zunächst eine Optimierung der Lernrate und anschließend des Momentum durch. Die Optimierungen werden zeichenweise von links nach rechts ausgeführt. Mögliche Parameter sind:

Parameter | automatische Optimierung
--------- | ------------------------
`e`       | maximale Fehlerrate
`l`       | Lernrate
`m`       | Momentum
`t`       | Topologie
`w`       | Bereich der Gewichteinitialisierung
