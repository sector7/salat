package de.fhw.salat.input.parser;


import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.fhw.salat.input.PatternProvider;

/**
 *
 * @author malte
 */
public class PatternFileParserTest {


    /**
     * Test method for {@link de.fhw.salat.input.PatternFileParser#getTrainingData()}.
     */
    @Test
    public final void testGetTrainingData() {
        assertTrue(PatternProvider.get("Trainingsdaten").size() == 260);
    }

}
