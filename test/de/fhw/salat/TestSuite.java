package de.fhw.salat;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.fhw.salat.input.parser.PatternFileParserTest;
import de.fhw.salat.params.ParamCalculatorTest;

@RunWith(Suite.class)
@SuiteClasses({
        PatternFileParserTest.class,
        ParamCalculatorTest.class
})
public class TestSuite {
}
