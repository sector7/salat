package de.fhw.salat.params;

import static de.fhw.salat.params.ParamCalculator.getRandomDoubleParam;
import static de.fhw.salat.params.ParamCalculator.getRandomIntParam;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ParamCalculatorTest {

    private boolean isIntInRange(int start, int end, int value) {
        return (start <= value) && (value <= end);
    }

    private void testRandomIntParam(int start, int end) {
        assertTrue(isIntInRange(start, end, getRandomIntParam(start, end)));
    }

    @Test
    public void getRandomIntParamTest() {
        testRandomIntParam(1, 4);
        testRandomIntParam(0, 0);
        testRandomIntParam(-1, 1);
    }

    private boolean isDoubleInRange(double start, double end, double value) {
        return (start <= value) && (value <= end);
    }

    private void testRandomDoubleParam(double start, double end) {
        assertTrue(isDoubleInRange(start, end, getRandomDoubleParam(start, end)));
    }

    @Test
    public void getRandomDoubleParamTest() {
        testRandomDoubleParam(0.0, 0.0);
        testRandomDoubleParam(0.0, 1.0);
        testRandomDoubleParam(-1.0, 1.0);
    }

}
