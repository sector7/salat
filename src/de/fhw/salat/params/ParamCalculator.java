package de.fhw.salat.params;

import java.util.List;
import java.util.Random;

import com.google.common.collect.ImmutableList;

/**
 * Stellt verschiedene Funktionen zur Belegung der Konfigurations-Parameter bereit.
 */
public final class ParamCalculator {

    /**
     * Zufallsgenerator
     */
    private static final Random random = new Random();

    /**
     * Liefert den Richtwert für die optimale Anzahl an inneren Neuronen.
     *
     * @param input int Anzahl der Input-Neuronen
     * @param output int Anzahl der Output-Neuronen
     * @return int
     */
    public static int getNumberOfInnerNeurons(int input, int output) {
        return (input + output) / 2 + 1;
    }

    /**
     * Liefert einen zufällige Ganzzahl im Bereich start..end
     *
     * @param start int Startwert
     * @param end int Endwert
     * @return int zufällige Ganzzahl
     */
    public static int getRandomIntParam(int start, int end) {
        return start + random.nextInt(end - start + 1);
    }

    /**
     * Liefert einen zufällige Ganzzahl im Bereich start..end, der sich an einem Richtwert
     * orientiert.
     *
     * @param start int Startwert
     * @param end int Endwert
     * @param approximateValue int Richtwert
     * @return int zufällige Ganzzahl, die sich an einem Richtwert orientiert
     */
    public static int getRandomIntParam(int start, int end, int approximateValue) {
        return (getRandomIntParam(start, end) + approximateValue) / 2;
    }

    /**
     * Liefert einen zufälligen boolean.
     *
     * @return boolean
     */
    public static boolean getRandomBooleanParam() {
        return random.nextBoolean();
    }

    /**
     * Liefert einen zufälligen Double Wert in einer bestimmten Range.
     *
     * @param start double Startwert
     * @param end double Endwert
     * @return double Zufallswert
     */
    public static double getRandomDoubleParam(double start, double end) {
        return random.nextDouble() * (end - start) + start;
    }

    /**
     * Liefert einen zufälligen Double Wert in einer bestimmten Range und berücksichtigt einen
     * bestimmten Richtwert.
     *
     * @param start double Startwert
     * @param end double Endwert
     * @param approximateValue double Richtwert
     * @return double Zufallswert
     */
    public static double getRandomDoubleParam(double start, double end, double approximateValue) {
        return (getRandomDoubleParam(start, end) + approximateValue) / 2.0;
    }

    /**
     * Liefert die Anzahl der Input-, inneren und Output-Neuronen in dieser Reihenfolge als Liste.
     *
     * @return List<Integer> Liste der Neuronen-Zahlen
     */
    public static List<Integer> getNeuronCountPerLayer(Params params) {
        return new ImmutableList.Builder<Integer>()
                .add(params.getInputNeuronCount())
                .addAll(params.getInnerNeuronCountPerLayer())
                .add(params.getOutputNeuronCount())
                .build();
    }
}
