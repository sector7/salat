package de.fhw.salat.params;

import static de.fhw.salat.params.ParamCalculator.getNumberOfInnerNeurons;

import java.util.Arrays;
import java.util.List;

import org.neuroph.util.TransferFunctionType;

/**
 * Default-Parameter zur Konfiguration des Netzwerkes.
 */
public final class DefaultParams {
    /**
     * Default Parameter, der optimiert wird
     */
    public static final ParamType defaultOptimizingParam = ParamType.NONE;

    /**
     * Default Anzahl der Eingabe-Neuronen
     */
    public static final int defaultInputNeuronCount = 196;
    /**
     * Anzahl der Ausgabe-Neuronen
     */
    public static final int defaultOutputNeuronCount = 26;

    /**
     * Anzahl der inneren Neuronen
     */
    public static final int defaultInnerNeuronCount = getNumberOfInnerNeurons(
            defaultInputNeuronCount, defaultOutputNeuronCount);

    /**
     * Anzahl der inneren Neuronen
     */
    public static final List<Integer> defaultInnerNeuronCountPerLayer = Arrays.asList(62);

    /**
     * Gewichte
     */
    public static final double defaultWeightsValue = 0.24d;

    /**
     * Einstellung, ob die Belegung der Gewichte zufällig geschehen soll.
     */
    public static final boolean defaultRandomizeWeights = true;

    /**
     * Maximalwert der Gewichte.
     */
    public static final double weightsValueRangeMax = 0.32d;

    /**
     * Mindestwert der Gewichte.
     */
    public static final double weightsValueRangeMin = 0.15d;

    /**
     * Verwendete Transferfunktion zwischen allen Schichten.
     */
    public static final TransferFunctionType defaultTransferFunctionType =
            TransferFunctionType.SIGMOID;

    /**
     * Einstellung für dynamische Anpassung des Momentums.
     */
    public static final boolean defaultDynamicMomentum = false;

    /**
     * Einstellung für dynamische Anpassung der Lernrate.
     */
    public static final boolean defaultDynamicLearningRate = false;

    /**
     * Trägheitswert
     */
    public static final double defaultMomentum = 0.63d;

    /**
     * Trägheitswert-Minimum
     */
    public static final double momentumMin = 0.55d;

    /**
     * Trägheitswert-Maximum
     */
    public static final double momentumMax = 0.68d;

    /**
     * Lernrate
     */
    public static final double defaultLearningRate = 0.65d;

    /**
     * Lernrate-Minimum
     */
    public static final double learningRateMin = 0.58d;

    /**
     * Lernrate-Maximum
     */
    public static final double learningRateMax = 0.68d;

    /**
     * Anzahl der Iterationen, nach denen das Lernen spätestens abgebrochen werden soll.
     */
    public static final int maxIterations = 20000;


    /**
     * Angabe, ob das aktuelle Lernverhalten epochenbasiert ist
     */
    public static final boolean defaultEpochLearningActivated = false;

    /**
     * Tolerierte Fehlerrate
     */
    public static final double defaultErrorRate = 0.015d;

    /**
     * Fehlerrate-Minimum
     */
    public static final double errorRateMin = 0.0075d;

    /**
     * Fehlerrate-Maximum
     */
    public static final double errorRateMax = 0.02d;


    /**
     * Trainingsdaten als Dateiliste
     */
    public static final List<String> defaultTrainingData = Arrays
            .asList("Trainingsdaten");

    /**
     * Testdaten als Dateiliste
     */
    public static final List<String> defaultTestData = Arrays
            .asList("TestWindowsSchrift",
                    "Trainingsdaten",
                    "TestHandschriften",
                    "Handschriften-Sommersemester-2015");

    /**
     * Schwellwert für Rückweisung
     */
    public static final double defaultConfidenceThreshold = 0.5d;
}
