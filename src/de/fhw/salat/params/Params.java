package de.fhw.salat.params;

import static de.fhw.salat.params.DefaultParams.defaultConfidenceThreshold;
import static de.fhw.salat.params.DefaultParams.defaultDynamicLearningRate;
import static de.fhw.salat.params.DefaultParams.defaultDynamicMomentum;
import static de.fhw.salat.params.DefaultParams.defaultEpochLearningActivated;
import static de.fhw.salat.params.DefaultParams.defaultErrorRate;
import static de.fhw.salat.params.DefaultParams.defaultInnerNeuronCount;
import static de.fhw.salat.params.DefaultParams.defaultInnerNeuronCountPerLayer;
import static de.fhw.salat.params.DefaultParams.defaultInputNeuronCount;
import static de.fhw.salat.params.DefaultParams.defaultLearningRate;
import static de.fhw.salat.params.DefaultParams.defaultMomentum;
import static de.fhw.salat.params.DefaultParams.defaultOptimizingParam;
import static de.fhw.salat.params.DefaultParams.defaultOutputNeuronCount;
import static de.fhw.salat.params.DefaultParams.defaultRandomizeWeights;
import static de.fhw.salat.params.DefaultParams.defaultTestData;
import static de.fhw.salat.params.DefaultParams.defaultTrainingData;
import static de.fhw.salat.params.DefaultParams.defaultTransferFunctionType;
import static de.fhw.salat.params.DefaultParams.defaultWeightsValue;
import static de.fhw.salat.params.DefaultParams.errorRateMax;
import static de.fhw.salat.params.DefaultParams.errorRateMin;
import static de.fhw.salat.params.DefaultParams.learningRateMax;
import static de.fhw.salat.params.DefaultParams.learningRateMin;
import static de.fhw.salat.params.DefaultParams.maxIterations;
import static de.fhw.salat.params.DefaultParams.momentumMax;
import static de.fhw.salat.params.DefaultParams.momentumMin;
import static de.fhw.salat.params.DefaultParams.weightsValueRangeMax;
import static de.fhw.salat.params.DefaultParams.weightsValueRangeMin;
import static de.fhw.salat.params.ParamCalculator.getRandomBooleanParam;
import static de.fhw.salat.params.ParamCalculator.getRandomDoubleParam;
import static de.fhw.salat.params.ParamCalculator.getRandomIntParam;

import java.util.ArrayList;
import java.util.List;

import org.neuroph.util.TransferFunctionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parameter zur Konfiguration des Netzwerkes.
 */
public class Params {


    private static final Logger LOG = LoggerFactory.getLogger("Params.class");

    /**
     * Aktuell optimierter Parameter
     */
    private ParamType optimizingParam = defaultOptimizingParam;

    /**
     * Anzahl der inneren Neuronen
     */
    private List<Integer> innerNeuronCountPerLayer = defaultInnerNeuronCountPerLayer;

    /**
     * Gewichte
     */
    private double weightsValue = defaultWeightsValue;

    /**
     * Einstellung, ob die Belegung der Gewichte zufällig geschehen soll.
     */
    private boolean randomizeWeights = defaultRandomizeWeights;

    /**
     * Dynamisches Momentum
     */
    private boolean dynamicMomentum = defaultDynamicMomentum;
    /**
     * Dynamische Lernrate
     */
    private boolean dynamicLearningRate = defaultDynamicLearningRate;
    /**
     * Transferfunktion
     */
    private TransferFunctionType transferFunctionType = defaultTransferFunctionType;
    /**
     * Trägheitswert
     */
    private double momentum = defaultMomentum;
    /**
     * Lernrate
     */
    private double learningRate = defaultLearningRate;
    /**
     * Angabe, ob das aktuelle Lernverhalten epochenbasiert ist
     */
    private boolean epochLearningActivated = defaultEpochLearningActivated;
    /**
     * Tolerierte Fehlerrate
     */
    private double errorRate = defaultErrorRate;

    /**
     * Liste der zum Training genutzten Daten
     */
    private List<String> trainingData = defaultTrainingData;

    /**
     * Schwellwert, ab dem Testergebnisse zurückgewiesen werden
     */
    private double confidenceThreshold = defaultConfidenceThreshold;

    /**
     * Liste der zum Testen genutzten Daten
     */
    private List<String> testData = defaultTestData;

    /**
     * Konstruktor zur Verwendung der Default-Parameter.
     */
    public Params() {
    }

    /**
     * Copy-Konstruktor zum duplizieren des übergebenen Parameter-Objekts
     *
     * @param params zu kopierendes Objekt
     */
    public Params(Params params) {
        this.optimizingParam = params.getOptimizingParam();
        this.innerNeuronCountPerLayer = new ArrayList<>(params.innerNeuronCountPerLayer);
        this.weightsValue = params.weightsValue;
        this.randomizeWeights = params.randomizeWeights;
        this.transferFunctionType = params.transferFunctionType;
        this.dynamicLearningRate = params.dynamicLearningRate;
        this.dynamicMomentum = params.dynamicMomentum;
        this.momentum = params.momentum;
        this.learningRate = params.learningRate;
        this.epochLearningActivated = params.epochLearningActivated;
        this.errorRate = params.errorRate;
        this.trainingData = new ArrayList<String>(params.trainingData);
        this.confidenceThreshold = params.confidenceThreshold;
        this.testData = new ArrayList<String>(params.testData);
    }

    /**
     * @return Liste der Dateinamen der Patternfiles
     */
    public List<String> getTrainingData() {
        return this.trainingData;
    }

    /**
     * Liefert die Anzahl der Input-Neuronen.
     *
     * @return int Anzahl
     */
    public int getInputNeuronCount() {
        return defaultInputNeuronCount;
    }

    /**
     * Liefert die Anzahl der Output-Neuronen.
     *
     * @return int Anzahl
     */
    public int getOutputNeuronCount() {
        return defaultOutputNeuronCount;
    }

    /**
     * Liefert die Anzahl der inneren Neuronen.
     *
     * @return int Anzahl
     */
    public List<Integer> getInnerNeuronCountPerLayer() {
        return this.innerNeuronCountPerLayer;
    }

    /**
     * Prüft, ob die gegebene innere Layernummer vorhanden ist.
     *
     * @param layer gesuchte Layernummer
     * @return <tt>true</tt>, wenn Layer vorhanden
     */
    public boolean hasLayer(int layer) {
        return this.innerNeuronCountPerLayer.size() > layer;
    }

    /**
     * Liefert die Anzahl der inneren Neuronen für ein bestimmtes Layer oder 0, falls Layer nicht
     * vorhanden ist.
     *
     * @param layer gesuchte Layernummer
     * @return die Anzahl der inneren Neuronen für ein bestimmtes Layer oder 0, falls Layer nicht
     *         vorhanden ist.
     */
    public int getInnerNeuronCountForLayer(int layer) {
        return hasLayer(layer) ? this.innerNeuronCountPerLayer.get(layer) : 0;
    }

    /**
     * Setzt die Anzahl der inneren Neuronen.
     *
     * @param int Anzahl
     * @return Params this
     */
    public Params setInnerNeuronCountPerLayer(List<Integer> innerNeuronCountPerLayer) {
        this.innerNeuronCountPerLayer = innerNeuronCountPerLayer;
        return this;
    }

    /**
     * Setzt die Anzahl der Neuronen für einen bestimmten Layer.
     *
     * @param layer int
     * @param count int
     * @return Params this
     */
    public Params setInnerNeuronCountForLayer(int layer, int count) {
        if (layer >= 0 && layer < this.innerNeuronCountPerLayer.size()) {
            this.innerNeuronCountPerLayer.set(layer, count);
        }
        return this;
    }

    /**
     * Liefert die Anzahl der Input-, inneren und Output-Neuronen in dieser Reihenfolge als Liste.
     *
     * @return List<Integer> Liste der Neuronen-Zahlen
     */
    public List<Integer> getNeuronCountPerLayer() {
        return ParamCalculator.getNeuronCountPerLayer(this);
    }

    /**
     * Liefert den Initialisierungswert der Gewichte
     *
     * @return double Initialisierungswert
     */
    public double getWeightsValue() {
        return this.weightsValue;
    }

    /**
     * Setzt den Initialisierungswert der Gewichte.
     *
     * @param double Initialisierungswert
     * @return Params this
     */
    public Params setWeightsValue(double weights) {
        this.weightsValue = weights;
        return this;
    }

    /**
     * Liefert die Einstellung, ob die Gewichte mit Zufallswerten belegt werden sollen.
     *
     * @return boolean
     */
    public boolean shouldRandomizeWeights() {
        return this.randomizeWeights;
    }

    /**
     * Setzt die Einstellung, ob die Gewichte mit Zufallswerten belegt werden sollen.
     *
     * @param defaultRandomizeWeights boolean Einstellung
     * @return Params this
     */
    public Params setRandomizeWeights(boolean randomizeWeights) {
        this.randomizeWeights = randomizeWeights;
        return this;
    }

    public boolean getDynamicMomentum() {
        return this.dynamicMomentum;
    }

    public void setDynamicMomentum(boolean dynamicMomentum) {
        this.dynamicMomentum = dynamicMomentum;
    }

    public boolean getDynamicLearningRate() {
        return this.dynamicLearningRate;
    }

    public void setDynamicLearningRate(boolean dynamicLearningRate) {
        this.dynamicLearningRate = dynamicLearningRate;
    }

    public TransferFunctionType getTransferFunctionType() {
        return this.transferFunctionType;
    }

    public void setTransferFunctionType(TransferFunctionType transferFunctionType) {
        this.transferFunctionType = transferFunctionType;
    }


    /**
     * Liefert den Trägheitswert.
     *
     * @return double Trägheitswert
     */
    public double getMomentum() {
        return this.momentum;
    }

    /**
     * Setzt den Trägheitswert der Gewichte.
     *
     * @param double Trägheitswert
     * @return Params this
     */
    public Params setMomentum(double momentum) {
        this.momentum = momentum;
        return this;
    }

    /**
     * Liefert die Lernrate.
     *
     * @return double Lernrate
     */
    public double getLearningRate() {
        return this.learningRate;
    }

    /**
     * Setzt die Lernrate.
     *
     * @param double Lernrate
     * @return Params this
     */
    public Params setLearningRate(double learningRate) {
        this.learningRate = learningRate;
        return this;
    }

    /**
     * Liefert die Information, ob epochenbasiertes Lernen aktiviert ist.
     *
     * @return boolean Einstellung epochenbasiertes Lernen
     */
    public boolean isEpochLearningActivated() {
        return this.epochLearningActivated;
    }

    /**
     * Setzt die Information, ob epochenbasiertes Lernen aktiviert ist.
     *
     * @param boolean Einstellung epochenbasiertes Lernen
     * @return Params this
     */
    public Params setEpochLearningActivated(boolean epochLearningActivated) {
        this.epochLearningActivated = epochLearningActivated;
        return this;
    }

    /**
     * Liefert die akzeptierte Fehlerrate.
     *
     * @return double akzeptierte Fehlerrate
     */
    public double getErrorRate() {
        return this.errorRate;
    }

    /**
     * Setzt die akzeptierte Fehlerrate.
     *
     * @param double akzeptierte Fehlerrate
     * @return Params this
     */
    public Params setErrorRate(double errorRate) {
        this.errorRate = errorRate;
        return this;
    }

    /**
     * Liefert den Schwellwert, der zur Ablehnung des Buchstaben führt
     *
     * @return Schwellwert, der zur Ablehnung des Buchstaben führt
     */
    public double getConfidenceThreshold() {
        return this.confidenceThreshold;
    }

    /**
     * Setzt den Schwellwert, der zur Ablehnung des Buchstaben führt
     *
     * @param Schwellwert, der zur Ablehnung des Buchstaben führt
     */
    public Params setConfidenceThreshold(double confidenceThreshold) {
        this.confidenceThreshold = confidenceThreshold;
        return this;
    }

    /**
     * Liefert die Dateinamen der zu verwendenden Testdaten
     *
     * @return Liste mit Dateinamen der zu verwendenden Testdaten
     */
    public List<String> getTestData() {
        return this.testData;
    }

    /**
     * Setzt die Parameter auf die Default-Parameter zurück.
     *
     * @return Params this
     */
    public Params resetToDefaultValues() {
        this.epochLearningActivated = defaultEpochLearningActivated;
        this.errorRate = defaultErrorRate;
        this.innerNeuronCountPerLayer = defaultInnerNeuronCountPerLayer;
        this.learningRate = defaultLearningRate;
        this.momentum = defaultMomentum;
        this.weightsValue = defaultWeightsValue;
        this.randomizeWeights = defaultRandomizeWeights;
        this.trainingData = defaultTrainingData;
        this.testData = defaultTestData;
        return this;
    }

    /**
     * Erzeugt zufällige Parameter-Werte mit Bezugname auf die Default-Werte.
     *
     * @return Params this
     */
    @Deprecated
    public Params randomize() {
        this.errorRate = getRandomDoubleParam(errorRateMin, errorRateMax, defaultErrorRate);
        for (int i = 0; i < this.innerNeuronCountPerLayer.size(); i++) {
            setInnerNeuronCountForLayer(i, getRandomIntParam(defaultInputNeuronCount,
                    defaultOutputNeuronCount, defaultInnerNeuronCount));
        }
        this.weightsValue =
                getRandomDoubleParam(weightsValueRangeMin, weightsValueRangeMax,
                        defaultWeightsValue);
        this.randomizeWeights = getRandomBooleanParam();
        this.learningRate =
                getRandomDoubleParam(learningRateMin, learningRateMax, defaultLearningRate);
        this.momentum = getRandomDoubleParam(momentumMin, momentumMax, defaultMomentum);
        return this;
    }

    /**
     * @return Anzahl der Iterationen, nach denen das Lernen spätestens abgebrochen werden soll.
     */
    public int getMaxIterations() {
        return maxIterations;
    }

    /**
     * Setzt einen Parameter des Typs double auf den übergebenen Wert.
     *
     * @param type Zu ändernder Parameter
     * @param value Neuer Wert für den Parameter
     */
    public void setDoubleParam(ParamType type, double value) {
        switch (type) {
        case CONFIDENCE_THRESHOLD:
            this.confidenceThreshold = value;
            break;
        case ERROR_RATE:
            this.errorRate = value;
            break;
        case LEARNING_RATE:
            this.learningRate = value;
            break;
        case MOMENTUM:
            this.momentum = value;
            break;
        case WEIGHTS_VALUE:
            this.weightsValue = value;
            break;
        default:
            LOG.warn("Nicht unterstützter double-Parametertyp: {}", type);
            break;
        }
    }

    /**
     * Gibt einen Parameter des Typs double zurück.
     *
     * @param type ausgewählter Parameter
     */
    public double getDoubleParam(ParamType type) {
        switch (type) {
        case CONFIDENCE_THRESHOLD:
            return this.confidenceThreshold;
        case ERROR_RATE:
            return this.errorRate;
        case LEARNING_RATE:
            return this.learningRate;
        case MOMENTUM:
            return this.momentum;
        case WEIGHTS_VALUE:
            return this.weightsValue;
        default:
            throw new IllegalArgumentException("Nicht unterstützter double-Parametertyp: " + type);
        }
    }

    /**
     * Liefert den aktuell optimierten Parametertyp.
     *
     * @return Aktuell optimierter Parametertyp
     */
    public ParamType getOptimizingParam() {
        return this.optimizingParam;
    }

    /**
     * Setzt den aktuell optimierten Parametertyp.
     *
     * @param p Aktuell optimierter Parametertyp
     */
    public void setOptimizingParam(ParamType p) {
        this.optimizingParam = p;
    }

}
