package de.fhw.salat.params;

/**
 * Enum zur Ermittlung einer Beschreibung der Parameter zu Dokumentationszwecken.
 */
public enum ParamType {
    TRANSFER_FUNCTION_TYPE("Transferfunktions-Typ"),
    CONFIDENCE_THRESHOLD("Rückweisungsschwellwert"),
    DETECTION_RATE("Erkennungsrate"),
    EPOCH_LEARNING_ACTIVATED("Epochenlernen"),
    ERROR_RATE("Fehlerrate"),
    INNER_NEURON_COUNT_PER_LAYER("Anzahl Neuronen Layer"),
    INPUT_NEURON_COUNT("Anzahl Inputneuronen"),
    DYNAMIC_LEARNING_RATE("Dynamische Lernrate"),
    LEARNING_RATE("Lernrate"),
    MAX_ITERATIONS("maximale Anzahl der Iterationen"),
    MAX_WEIGHTS_VALUE_RANGE("Maximalwert der Gewichte"),
    MIN_WEIGHTS_VALUE_RANGE("Minimalwert der Gewichte"),
    DYNAMIC_MOMENTUM("Dynamisches Momentum"),
    MOMENTUM("Momentum"),
    OUTPUT_NEURON_COUNT("Anzahl Outputneuronen"),
    RANDOMIZE_WEIGHTS("Zufällige Belegung der Gewichte"),
    TEST_DATA("Testdaten"),
    TRAINING_DATA("Trainingsdaten"),
    WEIGHTS_VALUE("Standardwert der Gewichte"),
    ITERATION_COUNT("Anzahl der Iterationen"),
    NONE("Kein");

    /** Beschreibung des ParamType */
    public final String description;

    /**
     * Konstruktorfunktion
     *
     * @param description Beschreibung
     */
    private ParamType(String description) {
        this.description = description;
    }
}
