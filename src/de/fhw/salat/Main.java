package de.fhw.salat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhw.salat.params.Params;
import de.fhw.salat.service.documentation.DocumentationService;
import de.fhw.salat.service.optimization.OptimizationService;

/**
 * Hauptklasse zur Erstellung und zum Testen eines künstlichen Neuronalen Netzwerks zur Erkennung
 * von lateinischen Großbuchstaben.
 *
 * @author Ellen Schwartau, Timo Gröger, Malte Jörgens
 */
public class Main {

    /** Logger für Konsolen-Ausgaben */
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    /**
     * Hauptprogramm: Initialisiert ein neuronales Netz mit Backpropagation zur Erkennung von
     * Handschriften. Zunächst beginnt der Lernprozess wird gestartet, nach dessen Abschluss die
     * Testphase initiiert wird.
     *
     * @param args Kommandozeilenargumente
     */
    public static void main(String[] args) {
        LOG.info("Starte Training des Künstlichen Neuronalen Netzwerkes...");
        DocumentationService.clearOutputDirectory();
        new OptimizationService(new Params()).optimize(args.length > 0 ? args[0] : "");
    }
}
