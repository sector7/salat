package de.fhw.salat.result;

import java.util.Arrays;
import java.util.List;

import de.fhw.salat.service.documentation.Documentable;

public class ResultStatistic extends Documentable {
    /** Zugrundeliegende Test-Dateien */
    private final String testFiles;

    /** Erkennungen */
    private final int detections;

    /** Falschzuweisungsrate */
    private final int falseDetections;

    /** Rückweisungsrate */
    private final int noDetections;

    /** Erkennungen */
    private final double detectionRate;

    /** Falschzuweisungsrate */
    private final double falseDetectionRate;

    /** Rückweisungsrate */
    private final double noDetectionRate;

    /** Gesamtzahl der durchgeführten Tests */
    private final int totalTestCount;

    /** Überschriften der Tabellenspalten für CSV-Export */
    static final List<String> TITLES = Arrays.asList("Testdateien", "Anzahl der Tests",
            "Anzahl Erkennung",
            "Erkennungsrate", "Anzahl Falscherkennung", "Falscherkennungsrate",
            "Anzahl Rückweisung", "Rückweisungsrate");

    /**
     * Konstruktorfunktion zur Erstellung einer Result-Statistik. Berechnet ausgehend von der Anzahl
     * der Erkennungen, Falscherkennungen und Rückweisungen die benötigten Raten.
     *
     * @param testFiles
     * @param detections
     * @param falseDetections
     * @param noDetections
     */
    public ResultStatistic(String testFiles, int detections, int falseDetections,
            int noDetections) {
        this.testFiles = testFiles;
        this.detections = detections;
        this.falseDetections = falseDetections;
        this.noDetections = noDetections;
        this.totalTestCount = this.detections + this.falseDetections + this.noDetections;
        this.noDetectionRate = calcRate(this.totalTestCount, this.noDetections) * 100;
        this.detectionRate = calcRate(this.totalTestCount, this.detections) * 100;
        this.falseDetectionRate = calcRate(this.totalTestCount, this.falseDetections) * 100;
    }

    /**
     * Konstruktorfunktion zur Erstellung einer Result-Statistik von einem Testdatensatz. Berechnet
     * ausgehend von der Anzahl der Erkennungen, Falscherkennungen und Rückweisungen die benötigten
     * Raten.
     *
     * @param testSetResult
     */
    public ResultStatistic(TestSetResult testSetResult) {
        this(testSetResult.getTestFile(), testSetResult.getDetections(), testSetResult
                .getFalseDetections(), testSetResult.getNoDetections());
    }

    /**
     * Berechnet das Verhältnis von count zu total.
     *
     * @param total Gesamtzahl
     * @param count Anzahl
     * @return Verhältnis von count zu total
     */
    private double calcRate(int total, int count) {
        if (total < count)
            throw new IllegalArgumentException("Count sollte kleiner als die totale Anzahl sein.");
        return (double) count / (double) total;
    }

    /**
     * Liefert die Namen der Testdateien.
     *
     * @return Dateinamen
     */
    public String getTestFiles() {
        return this.testFiles;
    }

    /**
     * Liefert die Gesamtzahl der durchgeführten Tests
     *
     * @return Anzahl der Tests
     */
    public int getTotalTestCount() {
        return this.totalTestCount;
    }

    /**
     * Liefert die Anzahl der Erkennungen.
     *
     * @return Anzahl
     */
    public int getDetections() {
        return this.detections;
    }

    /**
     * Liefert die Anzahl der Falscherkennungen.
     *
     * @return Anzahl
     */
    public int getFalseDetections() {
        return this.falseDetections;
    }

    /**
     * Liefert die Anzahl der Zurückweisungen.
     *
     * @return Anzahl
     */
    public int getNoDetections() {
        return this.noDetections;
    }

    /**
     * Liefert die Erkennungsrate
     * @return Rate
     */
    public double getDetectionRate() {
        return this.detectionRate;
    }

    /**
     * Liefert die Falscherkennungsrate
     * @return Rate
     */
    public double getFalseDetectionRate() {
        return this.falseDetectionRate;
    }

    /**
     * Liefert die Rückweisungsrate
     * @return Rate
     */
    public double getNoDetectionRate() {
        return this.noDetectionRate;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String
                .format("\nResultStatistic: [\ntestFiles:\t%s\n"
                        + "totalTestCount:\t%s\n"
                        + "detections:\t%s\n"
                        + "detectionRate:\t%s\n"
                        + "falseDetections:\t%s\n"
                        + "falseDetectionRate:\t%s\n"
                        + "noDetections:\t%s\n"
                        + "noDetectionRate:\t%s\n]\n\n",
                        this.testFiles, this.totalTestCount, this.detections, this.detectionRate,
                        this.falseDetections,
                        this.falseDetectionRate, this.noDetections, this.noDetectionRate);
    }

    @Override
    public List<String> getCSVRow() {
        return Arrays.asList(
                this.testFiles,
                Integer.toString(this.totalTestCount),
                Integer.toString(this.detections),
                Double.toString(this.detectionRate),
                Integer.toString(this.falseDetections),
                Double.toString(this.falseDetectionRate),
                Integer.toString(this.noDetections),
                Double.toString(this.noDetectionRate));
    }

    /*
     * Liefert die Spaltenüberschriften zur Dokumentation in einer CSV-Datei.
     */
    public static List<String> getRowTitles() {
        return TITLES;
    }

}
