package de.fhw.salat.result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Klasse zur Bewertung des Neuronalen Netzes. Ermittelt anhand von erwartetem und erhaltenem Output
 * mehrerer Testdaten-Sätze verschiedene Parameter zur Bewertung der Güte des Neuronalen Netzes.
 * Dazu zählen: Erkennungsrate, Falschzuweisungsrate, Rückweisungsrate
 */
public class TestRunResult {

    /** Liste mit allen bisher gesammelten Ergebnissen */
    private final List<TestSetResult> testSetResults = new ArrayList<>();

    /** Errechnete Ergebnisse */
    private Map<String, ResultStatistic> resultStatistics = null;

    /**
     * @return Liste mit allen bisher gesammelten Ergebnissen
     */
    public final List<TestSetResult> getTestSetResults() {
        return this.testSetResults;
    }

    /**
     * Berechnet die Erkennungs-, Fehler- und Rückweisungsrate anhand aller gesammelten Ergebnisse
     * neu
     *
     * @return Liste mit Ergebnis-Statistiken
     */
    public Map<String, ResultStatistic> evaluate() {
        if (this.resultStatistics != null)
            return this.resultStatistics;
        this.resultStatistics = new HashMap<>();
        int totalDetections = 0, totalFalseDetections = 0, totalNoDetections = 0;

        for (final TestSetResult testSetResult : this.testSetResults) {
            totalDetections += testSetResult.getDetections();
            totalFalseDetections += testSetResult.getFalseDetections();
            totalNoDetections += testSetResult.getNoDetections();
            this.resultStatistics.put(testSetResult.getTestFile(), new ResultStatistic(
                    testSetResult));
        }

        this.resultStatistics.put("GESAMT", new ResultStatistic("GESAMT", totalDetections,
                totalFalseDetections, totalNoDetections));
        return this.resultStatistics;
    }

    /**
     * Fügt ein Ergebnis zum Testlauf-Ergebnis hinzu.
     *
     * @param testSetResult
     */
    public void addTestSetResult(TestSetResult testSetResult) {
        this.testSetResults.add(testSetResult);

    }

    /**
     * Liefert den Qualitätsindex für diesen Testfall ausgehend von der Erkennungsrate des ersten
     * Testdatensatzes
     *
     * @return Erkennungsrate des ersten Testdatensatzes
     */
    public double getNetworkQualityIndex() {
        return evaluate().get("TestWindowsSchrift").getDetectionRate();
    }

    /**
     * Liefert die Testergebnisse für diesen Testfall für alle getesten Dateien.
     *
     * @return Erkennungsrate des ersten Testdatensatzes
     */
    public ResultStatistic getTestResult() {
        return evaluate().get("GESAMT");
    }

    /**
     * Liefert die Testergebnisse für diesen Testfall für einen bestimmten Datensatz.
     *
     * @return Erkennungsrate des ersten Testdatensatzes
     */
    public ResultStatistic getTestResult(String resultName) {
        return evaluate().get(resultName);
    }

}
