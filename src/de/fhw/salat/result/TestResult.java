package de.fhw.salat.result;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.fhw.salat.service.documentation.Documentable;

/**
 * Klasse zur Repräsentation genau eines Testergebnisses.
 */
public class TestResult extends Documentable {
    /** Erwarteter Buchstabe */
    private final char exp;
    /** Ermittelter Buchstabe */
    private final char res;
    /** Liste der ermittelten Übereinstummungsrate */
    private final double[] matchingRates;
    /** char wenn keiner erkannt wurde */
    public static final char NO_CHAR = '-';

    /** Überschriften der Tabellenspalten für CSV-Export */
    static final List<String> TITLES = new ArrayList<>();

    static {
        TITLES.addAll(Arrays.asList("Erwartet", "Best Guess"));
        for (char c = 'A'; c <= 'Z'; c++) {
            TITLES.add(Character.toString(c));
        }
    }

    /**
     * Konstruktorfunktion.
     *
     * @param exp Erwarteter Buchstabe
     * @param res Ermittelter Buchstabe
     * @param matchingRates Liste der ermittelten Übereinstummungsrate
     */
    public TestResult(char exp, char res, double[] matchingRates) {
        this.exp = exp;
        this.res = res;
        this.matchingRates = matchingRates;
    }

    /**
     * @return Liste der ermittelten Übereinstimmungsrate
     */
    public double[] getMatchingRates() {
        return this.matchingRates;
    }

    /**
     * Liefert den Ergebnistypen
     *
     * @return Typ: DETECTION | FALSE_DETECTION | NO_DETECTION
     */
    public TestResultType getResultType(){
        if (this.res == NO_CHAR)
            return TestResultType.NO_DETECTION;
        return (this.exp == this.res) ? TestResultType.DETECTION : TestResultType.FALSE_DETECTION;
    }

    @Override
    public List<String> getCSVRow() {
        final List<String> res = new ArrayList<>();
        res.add(Character.toString(this.exp));
        res.add(Character.toString(this.res));
        for (final double v : this.matchingRates) {
            res.add(Double.toString(v));
        }
        return res;
    }

    /**
     * Liefert die Überschriften der CSV-Datei.
     *
     * @return Überschriften
     */
    public static List<String> getRowTitles() {
        return TITLES;
    }

}
