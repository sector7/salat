package de.fhw.salat.result;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Klasse, die Testerebnisse eines einzelnen Testdatensatzes beinhaltet.
 */
public class TestSetResult {

    /** Liste der einzelnen Testergebnisse */
    private final List<TestResult> testResults = new ArrayList<>();

    /** Dateipfad der Testdatei */
    private final String testFile;


    /** Erkennungen */
    private int detections = 0;

    /** Falschzuweisungsrate */
    private int falseDetections = 0;

    /** Rückweisungsrate */
    private int noDetections = 0;

    /**
     * Liefert die Anzahl der Erkennungen.
     *
     * @return Anzahl
     */
    public int getDetections() {
        return this.detections;
    }

    /**
     * Liefert die Anzahl der Falscherkennungen.
     *
     * @return Anzahl
     */
    public int getFalseDetections() {
        return this.falseDetections;
    }

    /**
     * Liefert die Anzahl der Zurückweisungen.
     *
     * @return Anzahl
     */
    public int getNoDetections() {
        return this.noDetections;
    }

    /**
     * Konstruktor für das Testergebnis eines Testdatensatzes
     *
     * @param testFile Dateipfad der Testdatei
     */
    public TestSetResult(String testFile) {
        this.testFile = testFile;
    }

    /**
     * @return Dateipfad der Testdatei
     */
    public String getTestFile() {
        return this.testFile;
    }

    /**
     * @return Liste der einzelnen Testergebnisse
     */
    public List<TestResult> getTestResults() {
        return this.testResults;
    }

    /**
     * Fügt den Output des KNN als Testergebnis hinzu.
     *
     * @param result Ergebnis des Testdurchlaufs
     */
    public void addTestResult(TestResult result) {
        Objects.requireNonNull(result);
        this.testResults.add(result);
        switch (result.getResultType()) {
        case DETECTION:
            this.detections++;
            break;
        case FALSE_DETECTION:
            this.falseDetections++;
            break;
        case NO_DETECTION:
            this.noDetections++;
            break;
        }
    }

}
