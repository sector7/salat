package de.fhw.salat.result;

/**
 * Mögliche Test-Ergebnis-Typen.
 */
public enum TestResultType {
    /** Erkennung */
    DETECTION,
    /** Falscherkennung */
    FALSE_DETECTION,
    /** Rückweisung */
    NO_DETECTION
}
