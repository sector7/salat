package de.fhw.salat.util;

import java.io.IOException;
import java.util.EnumMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Klasse zum Plotten von CSV-Dateien.
 */
public class Plotter {

    /**
     * Darstellungstyp des Plotters.
     */
    public enum PlotterType {
        /** Darstellung als Linien-Graph */
        LINE,
        /** Darstellung als Punkte-Wolke */
        DOT
    }

    /** Logger zum erzeugen von Log-Ausgaben */
    private static final Logger LOG = LoggerFactory.getLogger(Plotter.class);

    /** Dateiname des Gnuplot-Skripts zur Erstellung von Liniengraphen */
    private static EnumMap<PlotterType, String> SCRIPTS = new EnumMap<>(PlotterType.class);

    static {
        SCRIPTS.put(PlotterType.LINE, "resource/plot-lines.pg");
        SCRIPTS.put(PlotterType.DOT, "resource/plot-linespoints.pg");
    }

    /**
     * Rendert die CSV-Datei als PNG mithilfe von GNUPlot
     */
    public static void createGraph(String csvFile, String outputFile, PlotterType type,
            String title, String titleX, String titleY) {
        try {
            LOG.debug("Erstelle Plot...");
            final Process proc =
                    new ProcessBuilder("gnuplot", "-e", "inputFile='" + csvFile
                            + "'; plotTitle='" + title
                            + "'; titleX='" + titleX
                            + "'; titleY='" + titleY
                            + "'; outputFile='" + outputFile + "'",
                            Plotter.SCRIPTS.get(type))
                            .start();
            proc.waitFor();
            if (proc.exitValue() != 0) {
                LOG.error("Fehler bei Erstellung des Plot!" + proc.getErrorStream().toString());
            } else {
                LOG.debug("Plot erfolgreich erstellt!");
            }
        } catch (final IOException | InterruptedException e) {
            LOG.error("Fehler bei Erstellung des Plot: {}", e.getMessage());
        }
    }
}
