package de.fhw.salat.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Allgemeine Klasse zur Erstellung CSV-artiger Textdateien
 */
public class DirectTableWriter {

    /** Datei, in die geschrieben wird. */
    private final FileWriter outputFile;
    /** Spaltenweises Trennzeichen */
    private final String valueSeperator;
    /** Zeilenweises Trennzeichen */
    private final String lineSeperator;

    /**
     * Konstruktor für eine neue CSV mit gegebenem Dateinamen und Standard-Trennzeichen.
     *
     * @param fileName Dateiname
     * @throws IOException
     */
    public DirectTableWriter(String fileName) throws IOException {
        this(fileName, ",", "\n");
    }

    /**
     * Leerer Konstruktor zur Initialisierung mit Standardwerten.
     *
     * @throws IOException
     */
    public DirectTableWriter(final String fileName, final String valueSeparator,
            final String lineSeperator) throws IOException {
        final File outputFile = new File(fileName);
        outputFile.getParentFile().mkdirs(); // Oberordner erstellen, falls noch nicht
                                                  // vorhanden
        this.outputFile = new FileWriter(outputFile);
        this.valueSeperator = valueSeparator;
        this.lineSeperator = lineSeperator;
    }

    /**
     * Konstruktor, der die Spaltenüberschriften festlegt
     *
     * @param header Sortierte Liste mit Spaltenüberschriften
     * @throws IOException
     */
    public DirectTableWriter(String fileName, List<String> header) throws IOException {
        this(fileName);
        writeHeader(header);
    }

    /**
     * @param header
     * @throws IOException
     */
    private void writeHeader(List<String> header) throws IOException {
        this.outputFile.write(String.join(this.valueSeperator, header));
        this.outputFile.write(this.lineSeperator);

    }

    /**
     * Konstruktor, der die Spaltenüberschriften festlegt
     *
     * @param header Spaltenüberschriften
     * @throws IOException
     */
    public DirectTableWriter(String fileName, String... header) throws IOException {
        this(fileName, Arrays.asList(header));
    }

    /**
     * Fügt eine zu schreibende Zeile hinzu.
     *
     * @param row neue Zeile
     * @return Referenz auf diese Instanz
     * @throws IOException
     */
    public DirectTableWriter addRow(List<String> row) throws IOException {
        this.outputFile.write(String.join(this.valueSeperator, row));
        this.outputFile.write(this.lineSeperator);
        this.outputFile.flush();
        return this;
    }

    /**
     * Fügt die übergebenen Einzelstrings der Liste der Reihen hinzu, die in eine Datei geschrieben
     * werden sollen.
     *
     * @param entries Einträge in einer Reihe
     * @return Referenz auf diese Instanz
     * @throws IOException
     */
    public DirectTableWriter addRow(String... entries) throws IOException {
        return addRow(Arrays.asList(entries));
    }

    /**
     * Fügt eine Liste an Zeilen den bisherigen Zeilen hinzu, die in eine Datei geschrieben werden
     * sollen.
     *
     * @param rows Liste an Zeilen
     * @return Referenz auf diese Instanz
     * @throws IOException
     */
    public DirectTableWriter addRows(List<List<String>> rows) throws IOException {
        for (final List<String> row : rows) {
            addRow(row);
        }
        return this;
    }
}
