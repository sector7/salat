package de.fhw.salat.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hilfsklasse zur Erstellung eines ZIP-Archivs des Ausgabeordners.
 */
public class ZipFolder {

    private static final Logger LOG = LoggerFactory.getLogger(ZipFolder.class);

    /**
     * Verpackt den angegebenen Ordner in ein ZIP-Archiv
     *
     * @param srcFolder Zu komprimierender Ordner
     * @param destZipFile Speicherort und -name des ZIP-Archivs
     * @throws IOException falls Dateien nicht gelesen oder geschrieben werden können
     */
    static public void zip(String srcFolder, String destZipFile) throws IOException {
        LOG.debug("Packe ZIP-Archiv {} ...", destZipFile);
        ZipOutputStream zip = null;
        FileOutputStream fileWriter = null;

        fileWriter = new FileOutputStream(destZipFile);
        zip = new ZipOutputStream(fileWriter);

        addFolderToZip("", srcFolder, zip); // Ordnerinhalt direkt in das root-Verzeichnis verpacken
        zip.flush();
        zip.close();
        LOG.debug("ZIP-Archiv erfolgreich erstellt!");
    }

    /**
     * Fügt eine einzelne Datei zum Archiv hinzu
     *
     * @param path Zielpfad im ZIP-Archiv
     * @param srcFile Hinzuzufügende Datei
     * @param zip ZIP-Stream, dem die Datei hinzugefügt wird
     * @throws IOException bei Schreib-/Lesefehlern
     */
    static private void addFileToZip(String path, String srcFile, ZipOutputStream zip)
            throws IOException {

        final File folder = new File(srcFile);
        if (folder.isDirectory()) {
            addFolderToZip(path, srcFile, zip);
        } else {
            final byte[] buf = new byte[1024];
            int len;
            final FileInputStream in = new FileInputStream(srcFile);
            zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
            while ((len = in.read(buf)) > 0) {
                zip.write(buf, 0, len);
            }
            in.close();
        }
    }

    /**
     * Fügt einen Ordner sowie seine beinhaltenden Dateien zum Archiv hinzu
     *
     * @param path Zielpfad im ZIP-Archiv
     * @param srcFolder Hinzuzufügender Ordner
     * @param zip ZIP-Stream, dem der Ordner hinzugefügt wird
     * @throws IOException bei Schreib-/Lesefehlern
     */
    static private void addFolderToZip(String path, String srcFolder, ZipOutputStream zip)
            throws IOException {
        final File folder = new File(srcFolder);

        // Alle beinhaltenden Dateien mit Ordnernamen als relativem Pfad hinzufügen
        for (final String fileName : folder.list()) {
            addFileToZip(path + (path.isEmpty() ? "" : "/" + folder.getName()), srcFolder + "/"
                    + fileName, zip);
        }
    }
}
