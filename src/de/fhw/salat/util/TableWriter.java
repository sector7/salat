package de.fhw.salat.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Allgemeine Klasse zur Erstellung CSV-artiger Textdateien
 */
public class TableWriter {

    /** Logger für Debug-Ausgaben */
    private static final Logger LOG = LoggerFactory.getLogger(TableWriter.class);
    /** Überschriften der Spalten, falls vorhanden */
    private final List<String> header;
    /** Zwischenspeicher der Zeilen */
    private final List<List<String>> rows = new LinkedList<>();

    /** Spaltenweises Trennzeichen */
    private String valueSeparator = ",";
    /** Zeilenweises Trennzeichen */
    private String lineSeparator = "\n";

    /**
     * Leerer Konstruktor zur Initialisierung mit Standardwerten.
     */
    public TableWriter() {
        this.header = null;
    }

    /**
     * Konstruktor, der die Spaltenüberschriften festlegt
     *
     * @param header Sortierte Liste mit Spaltenüberschriften
     */
    public TableWriter(List<String> header) {
        this.header = header;
    }

    /**
     * Konstruktor, der die Spaltenüberschriften festlegt
     *
     * @param header Spaltenüberschriften
     */
    public TableWriter(String... header) {
        this(Arrays.asList(header));
    }

    /**
     * Übernimmt neuen Spaltentrenner
     *
     * @param valueSeparator Trennung für Spalten
     */
    public final TableWriter setValueSeperator(String valueSeperator) {
        this.valueSeparator = valueSeperator;
        return this;
    }

    /**
     * Übernimmt neuen Zeilentrenner
     *
     * @param lineSeparator Trennung für Zeilen
     */
    public final TableWriter setLineSeperator(String lineSeperator) {
        this.lineSeparator = lineSeperator;
        return this;
    }

    /**
     * Fügt eine zu schreibende Zeile hinzu.
     *
     * @param row neue Zeile
     * @return Referenz auf diese Instanz
     */
    public TableWriter addRow(List<String> row) {
        this.rows.add(row);
        return this;
    }

    /**
     * Fügt die übergebenen Einzelstrings der Liste der Reihen hinzu, die in eine Datei geschrieben
     * werden sollen.
     *
     * @param entries Einträge in einer Reihe
     * @return Referenz auf diese Instanz
     */
    public TableWriter addRow(String... entries) {
        return addRow(Arrays.asList(entries));
    }

    /**
     * Fügt eine Liste an Zeilen den bisherigen Zeilen hinzu, die in eine Datei geschrieben werden
     * sollen.
     *
     * @param rows Liste an Zeilen
     * @return Referenz auf diese Instanz
     */
    public TableWriter addRows(List<List<String>> rows) {
        this.rows.addAll(rows);
        return this;
    }

    /**
     * Schreibt die vorhandenen Parameter in die Datei mit spezifiziertem Pfad und Dateinamen.
     *
     * @param name Name der Zieldatei
     */
    public void writeFile(String fileName) {
        final File csvFile = new File(fileName);
        if (csvFile.isDirectory())
            throw new IllegalArgumentException(csvFile.getAbsolutePath()
                    + " ist ein Verzeichnis!");
        csvFile.getParentFile().mkdirs(); // Oberordner erstellen, falls noch nicht vorhanden

        try (FileWriter writer = new FileWriter(csvFile)) {
            // Ggf. Header schreiben
            if (this.header != null) {
                writer.write(String.join(this.valueSeparator, this.header));
                writer.write(this.lineSeparator);
            }
            // Zeilen
            for (final List<String> row : this.rows) {
                writer.write(String.join(this.valueSeparator, row));
                writer.write(this.lineSeparator);
            }
        } catch (final IOException e) {
            LOG.error("Datei {} konnte nicht geschrieben werden: {}", csvFile.getPath(),
                    e.getMessage());
        }

    }
}
