package de.fhw.salat.service;

import java.util.Collection;

import org.neuroph.core.data.DataSet;
import org.neuroph.nnet.MultiLayerPerceptron;

import de.fhw.salat.input.PatternProvider;

/**
 * Service zum Trainieren eines Neuronalen Netzes.
 */
public class TrainNeuralNetworkService {

    /**
     * Trainiert ein neuronales Netz anhand einer Datei mit Trainingsdaten.
     *
     * @param neuralNetwork zu trainierendes Neuronales Netzwerk
     * @param path Pfad der Testdatei
     */
    public static final void trainNeuralNetwork(MultiLayerPerceptron neuralNetwork,
            Collection<String> fileNames) {
        final DataSet trainingSet = PatternProvider.get(fileNames);
        neuralNetwork.learn(trainingSet);
    }


}
