package de.fhw.salat.service.optimization;

public final class SampleTreeEntry {

    public final double detectionRate;
    public final int iterationCount;

    public SampleTreeEntry(double detectionRate, int iterationCount) {
        this.detectionRate = detectionRate;
        this.iterationCount = iterationCount;
    }

    @Override
    public String toString() {
        return "SampleTreeEntry [detectionRate=" + this.detectionRate + ", iterationCount="
                + this.iterationCount + "]";
    }

}
