package de.fhw.salat.service.optimization;

import static de.fhw.salat.params.DefaultParams.defaultInputNeuronCount;
import static de.fhw.salat.params.DefaultParams.defaultOutputNeuronCount;
import static de.fhw.salat.params.DefaultParams.errorRateMax;
import static de.fhw.salat.params.DefaultParams.errorRateMin;
import static de.fhw.salat.params.DefaultParams.learningRateMax;
import static de.fhw.salat.params.DefaultParams.learningRateMin;
import static de.fhw.salat.params.DefaultParams.momentumMax;
import static de.fhw.salat.params.DefaultParams.momentumMin;
import static de.fhw.salat.params.DefaultParams.weightsValueRangeMax;
import static de.fhw.salat.params.DefaultParams.weightsValueRangeMin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhw.salat.params.ParamType;
import de.fhw.salat.params.Params;
import de.fhw.salat.service.NeuralNetworkService;
import de.fhw.salat.service.documentation.DocumentationService;

/**
 * Klasse zur Optimierung der Parameter des neuronalen Netzes.
 *
 * Parameter Reihenfolge festlegen
 * <ul>
 * <li>getrennt zu betrachten: Epochenweises/Musterweises Lernen</li>
 * <li>Errorrate immer so klein wie möglich (limes -> 0), wird nur erhöht, wenn wir nicht zu einem
 * Ende kommen</li>
 * </ul>
 * <br/>
 * Anpassungsreihenfolge übrige Parameter
 * <ol>
 * <li>Musterweises/Epochenweises Lernen, nach Abschluss der Optimierung auf andere Methode
 * umschalten und vergleichen</li>
 * <li>Netzwerktopologie</li>
 * <li>Lernrate</li>
 * <li>Momentum</li>
 * </ol>
 */
public class OptimizationService {

    /** Anteil des zu optimierenden Bereiches */
    private static final double MIN_RANGE_SPLTTING_PORTION = 50.0d;

    /** Grundparameter, von denen ausgegangen wird */
    private final Params baseParams;

    /** Log */
    private static final Logger LOG = LoggerFactory.getLogger(OptimizationService.class);

    /** Konstante zur Anzahl der initialen Samples */
    private static final int BASE_SAMPLE_COUNT = 5;

    /** Service zur Verwaltung des NeuroNetzes */
    private NeuralNetworkService neuralNet = null;

    /**
     * Konstruktor für neuen Optimierungsprozess, der von übergebenen Parametern ausgeht.
     *
     * @param baseParams Basis-Parameter, von denen ausgehend optimiert wird
     * @param sampleCount Anzahl der zu Beginn zu erstellenden Samples
     * @param minimalStepWidth Minimal notwendiger Unterschied der Erkennungsrate
     */
    public OptimizationService(Params baseParams) {
        this.baseParams = baseParams;
        this.neuralNet = new NeuralNetworkService();
    }

    /**
     * Optimiert die übergebenen Parameter im Hinblick auf Netzwerktopologie, Lernrate und Momentum
     *
     * @param params Parameter, von denen ausgegangen wird
     */
    public void optimize(String args) {
        Params params = this.baseParams;
        ParamQuality paramQuality = new ParamQuality(params,
                this.neuralNet.doIteration(params),
                ParamType.NONE, 0.0d);
        for (int i = 0; i < args.length(); i++) {
            switch (args.charAt(i)) {
            case 't':
                // Topologie optimieren
                params = adjustTopology(this.baseParams);
                break;
            case 'e':
                // Fehlerrate optimieren
                paramQuality.setOptimizingParam(ParamType.ERROR_RATE);
                paramQuality.currentSampleKey = this.baseParams.getErrorRate();
                paramQuality = adjustErrorRate(paramQuality);
                break;
            case 'l':
                // Lernrate optimieren
                paramQuality.setOptimizingParam(ParamType.LEARNING_RATE);
                paramQuality.currentSampleKey = this.baseParams.getLearningRate();
                paramQuality = adjustLearningRate(paramQuality);
                break;
            case 'm':
                // Momentum optimieren
                paramQuality.setOptimizingParam(ParamType.MOMENTUM);
                paramQuality.currentSampleKey = this.baseParams.getMomentum();
                paramQuality = adjustMomentum(paramQuality);
                break;
            case 'w':
                // Gewichtebereich optimieren
                paramQuality.setOptimizingParam(ParamType.WEIGHTS_VALUE);
                paramQuality.currentSampleKey = this.baseParams.getWeightsValue();
                paramQuality = adjustWeightsValue(paramQuality);
                break;
            }
        }


    }

    /**
     * Verändert die Topologie des Netzwerks zur Findung einer guten Topologie.
     *
     * @param params Bisher beste Parameter
     * @return nach Optimierung gefundene beste Parameter
     */
    private Params adjustTopology(Params params) {
        double maxKPISoFar = Double.NEGATIVE_INFINITY;
        Params bestParamsSoFar = params;

        final Map<List<Integer>, Double> results = new HashMap<>();
        final int stepWidth =
                (defaultInputNeuronCount - defaultOutputNeuronCount) / 10;
        LOG.info(
                "Teste Topologiepermutationen mit {} Layern und Schrittweite {} im Bereich [{}, {}]",
                params.getInnerNeuronCountPerLayer().size(), stepWidth, defaultInputNeuronCount,
                defaultOutputNeuronCount);
        int thirdLayerNeurons = defaultOutputNeuronCount;
        do {
            if (params.hasLayer(2)) {
                params.setInnerNeuronCountForLayer(2, thirdLayerNeurons);
            }
            int secondLayerNeurons = defaultOutputNeuronCount;
            do {
                if (params.hasLayer(1)) {
                    params.setInnerNeuronCountForLayer(1, secondLayerNeurons);
                }
                int firstLayerNeurons = defaultOutputNeuronCount;
                do {
                    params.setInnerNeuronCountForLayer(0, firstLayerNeurons);
                    LOG.info("Teste mit Netzwerktopologie: {} | {} | {}", defaultInputNeuronCount,
                            params
                                    .getInnerNeuronCountPerLayer().toString(),
                            defaultOutputNeuronCount);
                    final double result = this.neuralNet.doIteration(params);
                    results.put(new ArrayList<>(params.getInnerNeuronCountPerLayer()), result);
                    if (result > maxKPISoFar) {
                        LOG.info("Bestes Ergebnis aktualisiert auf {}", result);
                        maxKPISoFar = result;
                        bestParamsSoFar = new Params(params);
                    }
                    firstLayerNeurons += stepWidth;
                } while (firstLayerNeurons <= defaultInputNeuronCount);
                secondLayerNeurons += stepWidth;
            } while (params.hasLayer(1)
                    && secondLayerNeurons <= defaultInputNeuronCount);
            thirdLayerNeurons += stepWidth;
        } while (params.hasLayer(2) && thirdLayerNeurons <= defaultInputNeuronCount);

        DocumentationService.documentTopology(results);
        return bestParamsSoFar;
    }

    /**
     * Sucht nach dem optimalen Fehlerrate
     *
     * @param paramQuality Bisher beste Parameter
     * @return nach Optimierung gefundene beste Parameter
     */
    private ParamQuality adjustErrorRate(ParamQuality paramQuality) {
        return adjustParameter(paramQuality, errorRateMin, errorRateMax);
    }

    /**
     * Sucht nach dem optimalen Momentum
     *
     * @param paramQuality Bisher beste Parameter
     * @return nach Optimierung gefundene beste Parameter
     */
    private ParamQuality adjustMomentum(ParamQuality paramQuality) {
        return adjustParameter(paramQuality, momentumMin, momentumMax);
    }

    /**
     * Sucht nach der optimalen Lernrate.
     *
     * @param paramQuality Bisher beste Parameter
     * @return nach Optimierung gefundene beste Parameter
     */
    private ParamQuality adjustLearningRate(ParamQuality bestParamQuality) {
        return adjustParameter(bestParamQuality, learningRateMin, learningRateMax);
    }

    /**
     * Sucht nach dem optimalen Initialisierungsbereich der Startgewichte.
     *
     * @param paramQuality Bisher beste Parameter
     * @return nach Optimierung gefundene beste Parameter
     */
    private ParamQuality adjustWeightsValue(ParamQuality bestParamQuality) {
        return adjustParameter(bestParamQuality, weightsValueRangeMin, weightsValueRangeMax);
    }

    /**
     * Versucht einen bestimmten Parameter im Bereich von min und max zu optimieren.
     *
     * @param paramQuality Bisher beste Parameter
     * @param max Minimalwert
     * @param min Maximalwert
     * @return nach Optimierung gefundene beste Parameter
     */
    private ParamQuality adjustParameter(ParamQuality paramQuality, double min, double max) {
        final String parameterDescr = paramQuality.getOptimizingParam().description;
        LOG.info("Optimiere {}...\n\n", parameterDescr);
        final SampleTree samples =
                new SampleTree(this.neuralNet, min, max, BASE_SAMPLE_COUNT,
                        (max - min) / MIN_RANGE_SPLTTING_PORTION, paramQuality);
        samples.findOptimalParameter();

        LOG.info("Optimale(s|r) {}: {} mit Erkennungsrate: {}", parameterDescr
                , samples.getBestSoFar().currentSampleKey
                , samples.getBestSoFar().detectionRate);
        LOG.info("Beende die Optimierung von {}...", parameterDescr);

        // Samples dokumentieren
        DocumentationService.documentSampleTree(samples);

        return samples.getBestSoFar();
    }

}
