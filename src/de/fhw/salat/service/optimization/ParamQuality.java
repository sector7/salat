package de.fhw.salat.service.optimization;

import de.fhw.salat.params.ParamType;
import de.fhw.salat.params.Params;

/**
 * Kapselnde Klasse zur Speicherung der bisher besten Parameter und deren Erkennungsrate.
 */
public class ParamQuality {

    /** Parameter, mit denen die bisher beste Erkennungsrate gefunden wurde */
    public final Params params;

    /** Bisher beste Erkennungsrate */
    public final double detectionRate;

    /**
     * @return Kennzeichnung des aktuell zu optimierenden ParamType
     */
    public final ParamType getOptimizingParam() {
        return this.params.getOptimizingParam();
    }

    /**
     * @param paramType Kennzeichnung des aktuell zu optimierenden ParamType
     */
    public final void setOptimizingParam(ParamType paramType) {
        this.params.setOptimizingParam(paramType);
    }

    /** Aktueller Parameterwert */
    public double currentSampleKey;

    /**
     * Speichert bisher beste Parameter und Erkennungsrate.
     *
     * @param params bisher beste Parameter
     * @param detectionRate bisher beste Erkennungsrate
     * @param paramType Typ des aktuell zu optimierenden ParamType
     * @param sampleKey Aktueller Wert des ParamType
     */
    public ParamQuality(Params params, double quality, ParamType paramDescription,
            double sampleKey) {
        this.params = params;
        this.detectionRate = quality;
        this.params.setOptimizingParam(paramDescription);
        this.currentSampleKey = sampleKey;
    }
}