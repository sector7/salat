package de.fhw.salat.service.optimization;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

import de.fhw.salat.params.ParamType;
import de.fhw.salat.params.Params;
import de.fhw.salat.service.NeuralNetworkService;
import de.fhw.salat.service.documentation.Documentable;

/**
 * Klasse zur Speicherung bisheriger Samples und zum Finden neuer geeigneter Testwerte.
 */
public class SampleTree extends Documentable {
    /** Log */
    private static final Logger LOG = LoggerFactory.getLogger(SampleTree.class);

    /** Bestes Ergebnis (bis jetzt!) */
    private ParamQuality bestParamQuality = null;

    /** Begrenzungen des Wertebereichs */
    private final double min, max;

    /** Minimale Schrittweite, die zum Abbruch des Optimierungsvorgangs unterschritten werden muss */
    private final double minimalStepWidth;

    /** Netzwerkverwaltung */
    private final NeuralNetworkService network;

    /** Anzahl der zu erzeugenden gleichverteilten Stichproben (sehr deutsch) */
    private final int sampleCount;

    /** Map, in der die Samples bestehend aus Wert -> Erkennungsrate gespeichert werden */
    private final TreeMap<Double, SampleTreeEntry> samples = new TreeMap<>();

    /** Schrittweite der Stichproben */
    private double stepWidth;

    /** Untersuchter Parametertyp */
    private final ParamType paramType;

    /**
     * Konstruktorfunktion für einen Sample Tree. Legt Bereich der validen Sample Keys fest.
     * @param min minimaler Key
     * @param max maximaler Key
     */
    public SampleTree(NeuralNetworkService network, double min, double max, int sampleCount,
            double minimalStepWidth, ParamQuality referenceParamQuality) {
        this.min = min;
        this.max = max;
        this.network = network;
        this.sampleCount = sampleCount;
        this.minimalStepWidth = minimalStepWidth;
        addEntry(referenceParamQuality.currentSampleKey, new SampleTreeEntry(
                referenceParamQuality.detectionRate, network.getLastTrainingIterationCount()));
        this.bestParamQuality = referenceParamQuality;
        this.paramType = referenceParamQuality.getOptimizingParam();
    }

    /**
     * Liefert den Parameter Typen, für den das Sampling durchgeführt wird.
     *
     * @return Typ des Parameters
     */
    public ParamType getParamType() {
        return this.paramType;
    }

    /**
     * Berechnet gleichverteilte Samples in einem angegebenen Bereich und liefert die berechneten
     * Samples mit den jeweiligen Erkennungsaten als Baumstruktur zurück
     *
     * @param params Parameter
     * @param min untere Schranke des zu samplenden Bereichs
     * @param max obere Schranke des zu samplenden Bereichs
     * @return Baumstruktur mit den Samples
     */
    private void baseSampling(final Params params) {
        // Initiale Suche nach Samples
        this.stepWidth = (this.max - this.min) / (this.sampleCount + 1);
        for (int sampleIndex = 1; sampleIndex <= this.sampleCount; sampleIndex++) {
            final double sampleKey = sampleIndex * this.stepWidth + this.min;
            // Trainings- und Testdurchlauf
            testAndAddValue(sampleKey, params);
        }
        LOG.info("Ermittelte Samples:\n{}", toString());
    }

    /**
     * Sucht das Optimum eines konkreten ParamType
     *
     * @param testParams bisher beste Testparameter
     */
    private void sampleRefinement(final Params testParams) {
        do {
            final double sampleKey = getBestSoFar().params.getDoubleParam(this.paramType);
            this.stepWidth /= 2.0d;
            LOG.info("Ausgewähltes Sample {} mit Schrittweite {}", sampleKey, this.stepWidth);
            final double leftSampleKey = Math.max(sampleKey - this.stepWidth, this.min);
            LOG.info("Teste linken Nachbarn bei {}", leftSampleKey);
            final double leftDetectionRate = testAndAddValue(leftSampleKey, testParams);
            final double rightSampleKey = sampleKey + this.stepWidth;
            LOG.info("Teste rechten Nachbarn bei {}", rightSampleKey);
            final double rightDetectionRate = testAndAddValue(rightSampleKey, testParams);
            LOG.debug("#######################");
            LOG.debug("#Linker  Test: ({},{})#", leftSampleKey, leftDetectionRate);
            LOG.debug("#Rechter Test: ({},{})#", rightSampleKey, rightDetectionRate);
            LOG.debug("#######################");
        } while (this.stepWidth >= this.minimalStepWidth);
    }

    /**
     * Führt einen Test mit dem gegebenen Sample-Key durch und speichert ihn im SampleTree. Die
     * aktuell besten Parameter werden aktualisiert.
     *
     * @param sampleKey Parameter-Wert mit dem getestet werden soll
     * @param samples bisher durchgeführte Tests
     * @param params Parameter
     * @return Erkennungsrate
     */
    private double testAndAddValue(double sampleKey, Params params) {
        final Params paramCopy = new Params(params);
        paramCopy.setDoubleParam(this.paramType, sampleKey);

        final ParamQuality currentQuality =
                new ParamQuality(paramCopy, this.network.doIteration(paramCopy),
                        this.paramType, sampleKey);
        addEntry(
                sampleKey,
                new SampleTreeEntry(currentQuality.detectionRate, this.network
                        .getLastTrainingIterationCount()));
        updateBestSoFar(currentQuality);

        return currentQuality.detectionRate;
    }

    /**
     * Überschreibt das biher beste Ergebnis, falls das neue Ergebnis eine höhere Erkennungsrate
     * aufweist.
     *
     * @param newDetectionRate neue Erkennungsrate
     * @param newParams Parameter, mit denen die Erkennungsrate gefunden wurde
     */
    private void updateBestSoFar(ParamQuality newParamQuality) {
        LOG.info("Neue ParamQuality: {}  >= aktuelle: {}, ", newParamQuality.detectionRate,
                this.bestParamQuality.detectionRate);
        if (newParamQuality.detectionRate >= this.bestParamQuality.detectionRate) {
            this.bestParamQuality = newParamQuality;
        }
    }

    /**
     * Speichert einen Sample im Tree. Eintrag besteht aus der Zuordnung eines Wertes zum Key.
     *
     * @param sampleKey Key
     * @param value Wert
     */
    public void addEntry(double sampleKey, SampleTreeEntry entry) {
        LOG.debug("Füge Sample ({},{}) ein.", sampleKey, entry.toString());
        this.samples.put(sampleKey, entry);
    }

    /**
     * @param params
     */
    public void findOptimalParameter() {
        baseSampling(this.bestParamQuality.params);
        sampleRefinement(this.bestParamQuality.params);
    }

    /**
     * @return bestes Zwischenergebnis
     */
    public ParamQuality getBestSoFar() {
        return this.bestParamQuality;
    }

    @Override
    public String toString() {
        return this.samples.toString();
    }

    @Override
    public List<String> getCSVRow() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<List<String>> getCSVRows() {
        final List<List<String>> csvRows = new ArrayList<List<String>>();
        for (final Map.Entry<Double, SampleTreeEntry> sample : this.samples.entrySet()) {
            final List<String> row = new ArrayList<String>();
            row.add(sample.getKey().toString());
            row.add(Double.toString(sample.getValue().detectionRate));
            row.add(Double.toString(sample.getValue().iterationCount));
            csvRows.add(row);
        }
        return csvRows;
    }

    /**
     * Liefert die Überschriften der CSV-Datei.
     *
     * @return Überschriften
     */
    public List<String> getRowTitles() {
        return ImmutableList.of(
                this.paramType.description, ParamType.DETECTION_RATE.description,
                ParamType.ITERATION_COUNT.description);
    }
}
