package de.fhw.salat.service;

import java.util.Arrays;

import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.DynamicBackPropagation;
import org.neuroph.util.NeuronProperties;
import org.neuroph.util.random.NguyenWidrowRandomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhw.salat.params.DefaultParams;
import de.fhw.salat.params.Params;

/**
 * Klasse zur Erstellung eines Neuronalen Netzes nach bestimmten Parametern. Setzt die gewünschten
 * Eigenschaften (Gewichte, Struktur, Lernmethode...)
 */
public class NeuralNetworkFactory {

    /** Logger für Konsolen-Ausgaben */
    private static final Logger LOG = LoggerFactory.getLogger(NeuralNetworkFactory.class);

    /**
     * Erstellt ein Neuronales Netzwerk nach den übergebenen Parametern.
     *
     * @param params Parameter
     * @param givenWeights Gewichtsbelegung, null wenn keine vorhanden
     * @return Neuronales Netzwerk
     */
    public static MultiLayerPerceptron createNeuralNetwork(Params params, double[] givenWeights) {
        // Neues Perzeptron-Netzwerk erstellen
        final MultiLayerPerceptron neuralNetwork = initializeNeuralNetwork(params);
        // Gewichte einstellen
        initializeWeights(neuralNetwork, params, givenWeights);
        // Lernregel erstellen und Parameter setzen
        neuralNetwork.setLearningRule(createLearningRule(params));

        return neuralNetwork;
    }

    /**
     * Erstellt ein neuronales Netzwerk nach den übergebenen Parametern.
     *
     * @param params Parameter
     * @return Neuronales Netzwerk
     */
    private static final MultiLayerPerceptron initializeNeuralNetwork(Params params) {
        LOG.info("Initialisiere Layer...");
        LOG.info("Input: {} Inner: {} Output: {}",
                params.getInputNeuronCount(),
                params.getInnerNeuronCountPerLayer(),
                params.getOutputNeuronCount()
                );
        LOG.info("Transferfunktion: {}", params.getTransferFunctionType());
        return new MultiLayerPerceptron(
                params.getNeuronCountPerLayer(),
                new NeuronProperties(params.getTransferFunctionType(), true));
    }

    /**
     * Erstellt Lernrgeln nach den übergebenen Parametern.
     *
     * @param params Params Parameter
     * @return DynamicBackPropagation
     */
    private static final DynamicBackPropagation createLearningRule(Params params) {
        LOG.info("Initialisiere das Neuronale Netzwerk...");
        final DynamicBackPropagation learningRule = new DynamicBackPropagation();
        learningRule.setMaxError(params.getErrorRate());

        learningRule.setUseDynamicLearningRate(params.getDynamicLearningRate());
        learningRule.setMaxLearningRate(DefaultParams.learningRateMax);
        learningRule.setMinLearningRate(DefaultParams.learningRateMin);
        learningRule.setLearningRate(params.getLearningRate());

        learningRule.setUseDynamicMomentum(params.getDynamicMomentum());
        learningRule.setMaxMomentum(DefaultParams.momentumMax);
        learningRule.setMinMomentum(DefaultParams.momentumMin);
        learningRule.setMaxIterations(params.getMaxIterations());

        learningRule.setBatchMode(params.isEpochLearningActivated());
        learningRule.setMomentum(params.getMomentum());

        LOG.info("Fehlerrate: {}", learningRule.getMaxError());
        LOG.info("Dynamische Lernrate: {}", learningRule.getUseDynamicLearningRate());
        LOG.info("Lernrate: {}",
                learningRule.getUseDynamicLearningRate() ? learningRule.getMinLearningRate()
                        + " - " + learningRule.getMaxLearningRate() : learningRule
                        .getLearningRate());
        LOG.info("Dynamisches Momentum: {}", learningRule.getUseDynamicMomentum());
        LOG.info("Momentum: {}",
                learningRule.getUseDynamicMomentum() ? learningRule.getMinMomentum()
                        + " - " + learningRule.getMaxMomentum() : learningRule
                        .getMomentum());
        LOG.info("Epochenlernen: {}", learningRule.isInBatchMode());

        return learningRule;
    }

    /**
     * Initialisiert die Gewichte des neuronalen Netzwerks
     *
     * @param neuralNetwork Neuronales Netzwerk
     * @param params Parameter
     * @param givenWeights Gewichtsbelegung, null wenn keine vorhanden
     */
    private static void initializeWeights(MultiLayerPerceptron neuralNetwork, Params params,
            double[] givenWeights) {
        if (givenWeights != null && givenWeights.length == neuralNetwork.getWeights().length) {
            LOG.info("Übernehme vorherige Gewichte...");
            neuralNetwork.setWeights(givenWeights);
        } else {
            final int weightCount = neuralNetwork.getWeights().length;
            if (params.shouldRandomizeWeights()) {
                LOG.info("Initialisiere {} Gewichte mit zufälligen Werten zwischen {} und {} ...",
                        weightCount,
                        -params.getWeightsValue(), params.getWeightsValue());
                // neuralNetwork
                neuralNetwork.randomizeWeights(new NguyenWidrowRandomizer(-params
                        .getWeightsValue(), params.getWeightsValue()));
            } else {
                LOG.info("Initialisiere {} Gewichte auf gegebenen Wert: {} ...",
                        weightCount,
                        params.getWeightsValue());
                final double[] weights = new double[weightCount];
                Arrays.fill(weights, params.getWeightsValue());
                neuralNetwork.setWeights(weights);
            }
        }
    }

}
