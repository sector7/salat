package de.fhw.salat.service;

import static de.fhw.salat.service.NeuralNetworkFactory.createNeuralNetwork;
import static de.fhw.salat.service.TestNeuralNetworkService.testNeuralNetwork;
import static de.fhw.salat.service.TrainNeuralNetworkService.trainNeuralNetwork;

import org.neuroph.nnet.MultiLayerPerceptron;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhw.salat.input.PatternProvider;
import de.fhw.salat.params.Params;
import de.fhw.salat.result.TestRunResult;
import de.fhw.salat.service.documentation.DocumentationService;

/**
 * Klasse zur Verwaltung und zur Ausführung von Iterationen und Optimierungen auf einem neuronalen
 * Netzwerk
 */
public class NeuralNetworkService {

    /** +- Bereich der Gewichteinitialisierung */
    private Double weightsRange = null;

    /** zwischengespeicherte Gewichte des neuronalen Netzes */
    private double[] weights = null;

    /** Anzahl der Trainingsiterationen des letzten Durchlaufes */
    private int lastTrainingIterationCount = 0;

    /** LOG */
    private static final Logger LOG = LoggerFactory.getLogger(NeuralNetworkService.class);

    /**
     * Konvertiert die Gewichte in das benötigte Format.
     *
     * @param weights Gewichte falsch formatiert
     * @return Gewichte richtig formatiert
     */
    private static double[] convertWeights(Double[] weights) {
        final double[] converted = new double[weights.length];
        for (int i = 0; i < weights.length; i++) {
            converted[i] = weights[i];
        }
        return converted;
    }

    /**
     * Trainiert und testet ein neuronales Netz auf Grundlage der übergebenen Parameter und
     * aktualisiert bisher bestes Ergebnis entsprechend
     *
     * @param params Parameterobjekt
     * @return Erkennungsrate des Durchlaufs
     */
    public double doIteration(Params params) {
        LOG.info("\n----------------------------------------------------------------");

        // Falls nicht erster Durchlauf oder Belegungsbereich verändert
        if (this.weightsRange == null
                || this.weightsRange.doubleValue() != params.getWeightsValue()) {
            this.weights = null;
            this.weightsRange = params.getWeightsValue();
        }

        // Neues Perzeptron-Netzwerk erstellen
        final MultiLayerPerceptron neuralNetwork = createNeuralNetwork(params, this.weights);

        // Falls nicht erster Durchlauf oder Anzahl der Gewichte verändert: Initialgewichte
        // zwischenspeichern
        if (this.weights == null || this.weights.length != neuralNetwork.getWeights().length) {
            this.weights = convertWeights(neuralNetwork.getWeights());
        }

        // Dokumentation aufnehmen
        final DocumentationService doc = new DocumentationService(neuralNetwork, params);

        // Trainings-Set erstellen und erlernen
        trainNeuralNetwork(neuralNetwork, params.getTrainingData());
        LOG.info("Fertig mit dem Training. Das KNN ist nun einsatzbereit!");

        // Test nach Training
        final TestRunResult testRunResult = testNeuralNetwork(
                neuralNetwork,
                PatternProvider.getByName(params.getTestData()),
                params.getConfidenceThreshold());

        doc.document(testRunResult);
        this.lastTrainingIterationCount = neuralNetwork.getLearningRule().getCurrentIteration();
        LOG.info("\n----------------------------------------------------------------\n");
        return testRunResult.getNetworkQualityIndex();
    }

    /**
     * Liefert die Anzahl der Trainingsiterationen des letzten Trainingsdurchlaufes.
     *
     * @return Anzahl der Trainingsiterationen
     */
    public int getLastTrainingIterationCount() {
        return this.lastTrainingIterationCount;
    }
}
