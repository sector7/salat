package de.fhw.salat.service.documentation;

import static de.fhw.salat.params.DefaultParams.defaultInputNeuronCount;
import static de.fhw.salat.params.DefaultParams.defaultOutputNeuronCount;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.neuroph.nnet.MultiLayerPerceptron;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhw.salat.listener.DynamicDataExporter;
import de.fhw.salat.listener.LearningListener;
import de.fhw.salat.listener.LearningRateExporter;
import de.fhw.salat.params.ParamType;
import de.fhw.salat.params.Params;
import de.fhw.salat.result.ResultStatistic;
import de.fhw.salat.result.TestResult;
import de.fhw.salat.result.TestRunResult;
import de.fhw.salat.result.TestSetResult;
import de.fhw.salat.service.optimization.SampleTree;
import de.fhw.salat.util.DirectTableWriter;
import de.fhw.salat.util.Plotter;
import de.fhw.salat.util.Plotter.PlotterType;
import de.fhw.salat.util.TableWriter;
import de.fhw.salat.util.ZipFolder;

/**
 * Klasse zur Dokumentation des Lernerfolgs des Neuronalen Netzes.
 */
public class DocumentationService {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentationService.class);

    /** Output Verzeichnis der Dokumentationsdateien */
    public static final String OUTPUT_DIR = "doc/out/current/";
    /** Name der Datei zur Dokumentation der Konfiguration des Neuronalen Netzes */
    private static final String DOC_CONFIGURATION_FILE_NAME = "configuration.csv";
    /** Name der Datei zur Dokumentation der Gewichte des Neuronalen Netzes */
    private static final String DOC_NEURAL_NETWORK_FILE_NAME = "neuralNetwork.nnet";
    /** Name der Datei zur Dokumentation der Gewichte des Neuronalen Netzes */
    private static final String DOC_WEIGHTS_FILE_NAME = "weights.csv";
    /** Name der Datei zur Dokumentation der Fehlerrate des Neuronalen Netzes */
    private static final String DOC_ERROR_RATE_FILE_NAME = "errorRate.csv";
    /** Name der Datei zur Dokumentation der dynamisch angepassten Daten */
    private static final String DOC_DYNAMIC_DATA = "dynamicData.csv";
    /** Präfix zur Dokumentation eines Sample Trees */
    private static final String DOC_SAMPLE_TREE_PRE = "optimize_";
    /** Präfix für geplottete Dateien */
    private static final String PLOT_PRE = "plot_";
    /** Suffix für geplottete Dateien */
    private static final String PLOT_SUF = ".png";
    /** Name der Datei zur Dokumentation der Testergebnisse */
    private static final String DOC_TEST_RESULTS = "testResults.csv";
    /** Dateiname für die Topologie-Übersicht */
    private static final String DOC_TOPOLOGY = "topology.csv";
    /** Name der Datei zur Dokumentation der Testergebnisse */
    private static final String DOC_TEST_SET_RESULT_PRE = "testSetResult_";
    /** Name der Dateiendung zur Dokumentation der Testergebnisse */
    private static final String DOC_TEST_SET_RESULT_SUF = ".csv";

    /** Name der CSV mit sämtlichen Testergebnissen. */
    private static final String DOC_ALL_TEST_RUNS = String.format(
            "doc/out/%1$ty-%1$tm-%1$td %1$tH-%1$tM", Calendar.getInstance()) + "_allTests.csv";

    private static DirectTableWriter allTests;

    static {
        try {
            allTests = new DirectTableWriter(DOC_ALL_TEST_RUNS, "Optimierungsphase",
                    "Input Neuronen", "Hidden Layer 1", "Hidden Layer 2", "Hidden Layer 3",
                    "Output Neuronen", "Gewichte-Belegungsbereich +-", "Lernrate", "Momentum",
                    "Tolerierte Fehlerrate",
                    "Erreichte Fehlerrate", "Iterationen", "Erkennungsrate",
                    "Rückweisungsrate", "Falschzuweisungsrate", "CSV-Datei");
        } catch (final IOException e) {
            System.err.println("Error creating the overview CSV: " + e.getLocalizedMessage());
            System.exit(-1);
        }
    }


    /** Neuronales Netz, dessen Eigenschaften dokumentiert werden sollen */
    private final MultiLayerPerceptron neuralNetwork;

    /** Konfiguration der Netzwerkes */
    private final Params params;

    /** ursprüngliche Einstellung der Gewichte vor der Testphase */
    private final Double[] initialWeights;

    /**
     * Konstruktorfunktion
     *
     * @param neuralNetwork MomentumBackpropagation
     */
    public DocumentationService(MultiLayerPerceptron neuralNetwork, Params params) {
        this.neuralNetwork = neuralNetwork;
        this.initialWeights = neuralNetwork.getWeights();
        this.params = params;
        neuralNetwork.getLearningRule().addListener(new LearningListener());
        neuralNetwork.getLearningRule().addListener(
                new LearningRateExporter(OUTPUT_DIR + DOC_ERROR_RATE_FILE_NAME));
        if (params.getDynamicLearningRate() || params.getDynamicMomentum()) {
            neuralNetwork.getLearningRule().addListener(
                    new DynamicDataExporter(OUTPUT_DIR + DOC_DYNAMIC_DATA));
        }
    }

    /**
     * Stößt die Dokumentation des Neuronalen Netzes an.
     */
    public void document(TestRunResult result) {
        documentConfiguration();
        documentWeights();
        // documentNeuralNetwork(); Auskommentiert, erzeugt StackOverflow beim Schreiben großer
        // Schichten
        documentTestSetResults(result.getTestSetResults());
        documentTestRunResult(result);

        final String csvName = String.format("doc/out/%1$ty-%1$tm-%1$td %1$tH-%1$tM-%1$tS.zip",
                Calendar.getInstance());

        // "Input Neuronen", "Hidden Layer 1", "Hidden Layer 2", "Hidden Layer 3",
        // "Output Neuronen", "Lernrate", "Momentum", "Tolerierte Fehlerrate",
        // "Erreichte Fehlerrate", "Iterationen", "Erkennungsrate",
        // "Rückweisungsrate", "Falschzuweisungsrate", "CSV-File"
        try {
            allTests.addRow(
                    this.params.getOptimizingParam().description,
                    Integer.toString(this.params.getInputNeuronCount()),
                    Integer.toString(this.params.getInnerNeuronCountForLayer(0)),
                    Integer.toString(this.params.getInnerNeuronCountForLayer(1)),
                    Integer.toString(this.params.getInnerNeuronCountForLayer(2)),
                    Integer.toString(this.params.getOutputNeuronCount()),
                    Double.toString(this.params.getWeightsValue()),
                    Double.toString(this.params.getLearningRate()),
                    Double.toString(this.params.getMomentum()),
                    Double.toString(this.params.getErrorRate()),
                    Double.toString(this.neuralNetwork.getLearningRule().getTotalNetworkError()),
                    Integer.toString(this.neuralNetwork.getLearningRule().getCurrentIteration()),
                    Double.toString(result.getNetworkQualityIndex()),
                    Double.toString(result.getTestResult("TestWindowsSchrift").getNoDetectionRate()),
                    Double.toString(result.getTestResult("TestWindowsSchrift")
                            .getFalseDetectionRate()),
                    csvName);
        } catch (final IOException e1) {
            LOG.error("IO-Fehler beim Schreiben der Übersichtsdatei: ", e1);
        }

        // Alle erstellten Inhalte in ein ZIP-Archiv verpacken
        try {
            ZipFolder.zip("doc/out/current", csvName);
        } catch (final IOException e) {
            LOG.error("Konnte ZIP-Archiv nicht erstellen: {}", e.getMessage());
        }
    }

    /**
     * Dokumentiert die Konfiguration der neuronalen Netzes.
     */
    private void documentConfiguration() {
        final TableWriter writer = new TableWriter("Parameter", "Wert");
        writer.setValueSeperator("\t");
        writer.addRow(ParamType.EPOCH_LEARNING_ACTIVATED.description,
                Boolean.toString(this.params.isEpochLearningActivated()))
                .addRow(ParamType.ERROR_RATE.description,
                        Double.toString(this.params.getErrorRate()))
                .addRow(ParamType.TRANSFER_FUNCTION_TYPE.description,
                        this.params.getTransferFunctionType().toString())
                .addRow(ParamType.INNER_NEURON_COUNT_PER_LAYER.description,
                        this.params.getInnerNeuronCountPerLayer().toString())
                .addRow(ParamType.INPUT_NEURON_COUNT.description,
                        Integer.toString(this.params.getInputNeuronCount()))
                .addRow(ParamType.DYNAMIC_LEARNING_RATE.description,
                        Boolean.toString(this.params.getDynamicLearningRate()))
                .addRow(ParamType.LEARNING_RATE.description,
                        Double.toString(this.params.getLearningRate()))
                .addRow(ParamType.MAX_ITERATIONS.description,
                        Integer.toString(this.params.getMaxIterations()))
                .addRow(ParamType.MAX_WEIGHTS_VALUE_RANGE.description,
                        Double.toString(this.params.getWeightsValue()))
                .addRow(ParamType.MIN_WEIGHTS_VALUE_RANGE.description,
                        Double.toString(-this.params.getWeightsValue()))
                .addRow(ParamType.DYNAMIC_MOMENTUM.description,
                        Boolean.toString(this.params.getDynamicMomentum()))
                .addRow(ParamType.MOMENTUM.description,
                        Double.toString(this.params.getMomentum()))
                .addRow(ParamType.OUTPUT_NEURON_COUNT.description,
                        Integer.toString(this.params.getOutputNeuronCount()))
                .addRow(ParamType.RANDOMIZE_WEIGHTS.description,
                        Boolean.toString(this.params.shouldRandomizeWeights()))
                .addRow(ParamType.WEIGHTS_VALUE.description,
                        Double.toString(this.params.getWeightsValue()))
                .addRow(ParamType.CONFIDENCE_THRESHOLD.description,
                        Double.toString(this.params.getConfidenceThreshold()))
                .addRow(ParamType.TRAINING_DATA.description,
                        this.params.getTrainingData().toString())
                .addRow(ParamType.TEST_DATA.description,
                        this.params.getTestData().toString())
                .writeFile(OUTPUT_DIR + DOC_CONFIGURATION_FILE_NAME);
    }

    /**
     * Dokumentiert die Anfangs- und End-Einstellung der Gewichte
     */
    private void documentWeights(){
        final TableWriter writer =
                new TableWriter("Ursprungs-Gewicht", "End-Gewicht");
        final Double[] endWeights = this.neuralNetwork.getWeights();

        for (int i = 0; i < endWeights.length; i++) {
            writer.addRow(this.initialWeights[i].toString(), endWeights[i].toString());
        }

        writer.writeFile(OUTPUT_DIR + DOC_WEIGHTS_FILE_NAME);
    }

    /**
     * Schreibt das gesamte Neuronale Netz in eine Datei
     */
    private void documentNeuralNetwork() {
        this.neuralNetwork.save(OUTPUT_DIR + DOC_NEURAL_NETWORK_FILE_NAME);
    }

    /**
     * Dokumentiert die Testergebnisse.
     */
    public void documentTestRunResult(TestRunResult testRunResult) {
        final TableWriter writer =
                new TableWriter(ResultStatistic.getRowTitles()).setValueSeperator(";");
        for (final Documentable result : testRunResult.evaluate().values()) {
            writer.addRow(result.getCSVRow());
            LOG.debug(result.toString());
        }

        LOG.info("Erkennungsrate Windows-Schriften: {}", testRunResult.getNetworkQualityIndex());
        writer.writeFile(OUTPUT_DIR + DOC_TEST_RESULTS);
    }

    /**
     * Dokumentiert ein TestSet
     *
     * @param testSetResult zu dokumentierendes TestSet
     */
    public void documentTestSetResults(List<TestSetResult> testSetResult) {
        for (final TestSetResult setResult : testSetResult) {
            final TableWriter writer =
                    new TableWriter(TestResult.getRowTitles()).setValueSeperator(";");
            for (final Documentable result : setResult.getTestResults()) {
                writer.addRow(result.getCSVRow());
            }

            writer.writeFile(OUTPUT_DIR + DOC_TEST_SET_RESULT_PRE + setResult.getTestFile()
                    + DOC_TEST_SET_RESULT_SUF);
        }
    }

    /**
     * Dokumentiert einen Sample-Tree nach dem Optimierungsvorgang eines bestimmten Parameters.
     */
    public static void documentSampleTree(SampleTree sampleTree) {
        final String paramName = sampleTree.getParamType().description;
        final String csvFile =
                OUTPUT_DIR + DOC_SAMPLE_TREE_PRE + paramName + DOC_TEST_SET_RESULT_SUF;
        final String plotFile =
                OUTPUT_DIR + PLOT_PRE + paramName + PLOT_SUF;
        final TableWriter writer =
                new TableWriter(sampleTree.getRowTitles()).setValueSeperator(",");
        writer.addRows(sampleTree.getCSVRows());
        writer.writeFile(csvFile);
        Plotter.createGraph(csvFile, plotFile, PlotterType.DOT,
                "Optimierung: " + paramName, paramName,
                ParamType.DETECTION_RATE.description);
    }

    /**
     * Löscht alle bisher vorhandenen Dateien aus dem Ausgabe-Ordner.
     */
    public static void clearOutputDirectory() {
        final File folder = new File(OUTPUT_DIR);
        final File[] files = folder.listFiles();
        if (files != null) {
            for (final File f : files) {
                f.delete();
            }
        }
    }

    /**
     * Speichert die Ergebnisse der Topologieänderungen als CSV-Datei.
     *
     * @param results
     */
    public static void documentTopology(Map<List<Integer>, Double> results) {
        // Überschriften erstellen
        final List<String> headlines = new ArrayList<>();
        headlines.add("Input Neuronen");
        headlines.add("Output Neuronen");
        for (int layer = 1; layer <= results.keySet().iterator().next().size(); layer++) {
            headlines.add("Hidden Layer " + layer);
        }
        headlines.add("Erkennungsrate");

        // Zeilen füllen
        final String innerNeurons = new Double(defaultInputNeuronCount).toString();
        final String outputNeurons = new Double(defaultOutputNeuronCount).toString();
        final TableWriter writer = new TableWriter(headlines).setValueSeperator("\t");
        for (final Entry<List<Integer>, Double> entry : results.entrySet()) {
            final List<String> row = new ArrayList<>();
            row.add(innerNeurons);
            row.add(outputNeurons);
            row.addAll(entry.getKey().stream().map(e -> e.toString()).collect(Collectors.toList()));
            row.add(entry.getValue().toString());
            writer.addRow(row);
        }
        writer.writeFile(OUTPUT_DIR + DOC_TOPOLOGY);
    }

}
