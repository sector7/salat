package de.fhw.salat.service.documentation;

import java.util.ArrayList;
import java.util.List;

/**
 * Schnittstelle zur Ausgabe eines Datensatzes als CSV-Zeile als Liste von Strings
 */
public abstract class Documentable {

    /**
     * Einzelne CSV-Zeile dieses Datensatzes
     *
     * @return Einzelne CSV-Zeile dieses Datensatzes
     */
    public abstract List<String> getCSVRow();

    /**
     * Liefert alle CSV-Zeilen des Datensatzes
     *
     * @return alle CSV-Zeilen
     */
    public List<List<String>> getCSVRows() {
        final List<List<String>> csvRows = new ArrayList<List<String>>();
        csvRows.add(getCSVRow());
        return csvRows;
    }

}
