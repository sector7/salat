/**
 *
 */
package de.fhw.salat.service;

import java.text.DecimalFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import de.fhw.salat.params.DefaultParams;
import de.fhw.salat.result.TestResult;

/**
 *
 * @author Ellen Schwartau, Timo Gröger, Malte Jörgens
 */
public class OutputManager {

    /** Original-Output eines NN */
    List<Map.Entry<Character, Double>> originalOutputList = new ArrayList<>();

    /** Sortierter Output eines NN */
    List<Map.Entry<Character, Double>> sortedOutputList = null;

    public OutputManager() {

    }

    /**
     * Erzeugt einen Output-Manager mit einer Ergebnisliste eines NN.
     *
     * @param output Einzulesende Ergebnisliste
     */
    public OutputManager(double output[]) {
        setOutput(output);
    }

    /**
     * Setzt die Ergebnisliste des Formatters.
     *
     * @param output In den Formatter einzulesende Ergebnisliste des NN.
     */
    public void setOutput(double output[]) {
        this.originalOutputList = parseNNOutputToList(output);
        this.sortedOutputList = null;
    }

    /**
     * Liefert die aktuelle Liste mit Outputs. Sie enthält Tupel in der Form (Char, Double).
     *
     * @return Output-Liste
     */
    public List<Map.Entry<Character, Double>> getOriginalOutput() {
        return this.originalOutputList;
    }

    /**
     * Liest den Output eines NN in Form eines Arrays ein und generiert daraus eine unsortierte
     * Liste mit Buchstabe-Wert-Tupeln.
     *
     * @param output Enthält den gewünschten oder erzeugten Output eines NN
     * @return Liste mit Tupeln von Buchstabe auf Wert
     */
    private List<Map.Entry<Character, Double>> parseNNOutputToList(double output[]) {
        final List<Map.Entry<Character, Double>> res =
                new ArrayList<Map.Entry<Character, Double>>();
        for (int i = 0; i < output.length; i++) {
            res.add(new AbstractMap.SimpleEntry<Character, Double>((char) (i + 'A'), output[i]));
        }
        return res;
    }

    /**
     * Formatiert die unsortierte Ergebnisliste lesbar und gibt diese als String zurück.
     *
     * @return Unsortierte Ergebnisliste als lesbarer String
     */
    public String prettyPrintOriginal() {
        return pp(this.originalOutputList);
    }

    /**
     * Formatiert die sortierte Ergebnisliste lesbar und gibt diese als String zurück.
     *
     * @return Sortierte Ergebnisliste als lesbarer String
     */
    public String prettyPrintSorted() {
        return pp(this.sortedOutputList);
    }

    /**
     * Formatiert eine Liste zu einem lesbaren String.
     *
     * @param l Zu formatierende Liste
     * @return Formatierte Liste in Stringrepräsentation
     */
    private String pp(List<Map.Entry<Character, Double>> l) {
        final DecimalFormat df = new DecimalFormat("##.##");
        final StringBuilder res = new StringBuilder();

        if (l != null) {
            for (final Map.Entry<Character, Double> entry : l) {
                res.append(entry.getKey()).append("=").append(df.format(entry.getValue() * 100))
                        .append("% ");
            }
        }
        return res.toString();
    }

    /**
     * Sortiert die Liste der Ergebnisse des NN absteigend nach ihrer Wahrscheinlichkeit.
     */
    public void sort() {
        if (this.sortedOutputList == null && this.originalOutputList != null) {
            this.sortedOutputList = new ArrayList<>();
            // Unsortierte Liste kopieren
            this.sortedOutputList.addAll(this.originalOutputList);

            Collections.sort(this.sortedOutputList, new OutputComparator());
            Collections.reverse(this.sortedOutputList);
        }
    }

    /**
     * Liefert den am wahrscheinlichsten eingelesenen Buchstaben, falls dieser oberhalb des
     * eingegebenen Schwellenwertes liegt. Liefert ansonsten NO_CHAR.
     *
     * @param confidenceThreshold Schwellenwert, der zur Ausgabe eines wahrscheinlichsten
     *            Buchstabens erreicht sein muss
     * @return Wahrscheinlichster Buchstabe oder NO_CHAR
     * @post Output-Liste ist sortiert
     */
    public Map.Entry<Character, Double> getBestGuess(double confidenceThreshold) {
        sort();
        if (this.sortedOutputList.get(0).getValue() >= confidenceThreshold)
            return this.sortedOutputList.get(0);
        else
            return new AbstractMap.SimpleEntry<Character, Double>(TestResult.NO_CHAR, 100.0);
    }

    /**
     * Liefert den am wahrscheinlichsten eingelesenen Buchstaben.
     *
     * @return Wahrscheinlichster Buchstabe oder NO_CHAR
     * @post Output-Liste ist sortiert
     */
    public Map.Entry<Character, Double> getBestGuess() {
        return getBestGuess(DefaultParams.defaultConfidenceThreshold);
    }


    /**
     * Liefert die Wahrscheinlichkeit, mit der der Input einem bestimmten Buchstaben entspricht.
     * Wird ein ungültiger Buchstabe eingegeben oder der Buchstabe nicht gefunden, liefert die
     * Methode -1.
     *
     * @pre Im OutputManager geladene Liste bildet ein vollständiges Alphabet ab.
     * @param c Zu prüfender Buchstabe
     * @return Wahrscheinlichkeit
     */
    public double getConfidenceForChar(char c) {
        double res = 0;
        try {
            // Indizierter Zugriff ist schnell, wirft Exception bei Abfrage nach nicht-
            // vorhandenem Buchstaben.
            res = this.originalOutputList.get(Character.getNumericValue(c) - Character.getNumericValue('A')).getValue();
        } catch (final IndexOutOfBoundsException e) {
            res = -1.0;
        }

        return res;
    }

    /**
     * Innere Klasse zum Vergleichen verschiedener Ausgaben des Neuronalen Netzes.
     *
     * @author Ellen Schwartau, Timo Gröger, Malte Jörgens
     *
     */
    class OutputComparator implements Comparator<Map.Entry<Character, Double>> {
        @Override
        public int compare(Map.Entry<Character, Double> o1, Map.Entry<Character, Double> o2) {
            return o1.getValue().compareTo(o2.getValue());
        }
    }
}
