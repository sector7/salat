package de.fhw.salat.service;

import java.util.Map;

import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhw.salat.result.TestResult;
import de.fhw.salat.result.TestRunResult;
import de.fhw.salat.result.TestSetResult;

/**
 * Service zum Testen einse Neuronalen Netzes. Lässt verschiedene Testsätze auf das Neuronale Netz
 * los und evaluiert den Gehalt der Ergebnisse.
 */
public class TestNeuralNetworkService {
    /** Logger für Konsolen-Ausgaben */
    private static final Logger LOG = LoggerFactory.getLogger(TestNeuralNetworkService.class);
    /** Verwaltende Instanz der Testdaten */
    private static final OutputManager desiredOutputManager = new OutputManager();
    /** Verwaltende Instanz des Outputs vom NN */
    private static final OutputManager nnOutputManager = new OutputManager();

    /**
     * Testet die Funktionalität des neuronalen Netzes nach Abschluss der Trainingsphase.
     *
     * @param neuralNetwork neuronales Netz
     * @param testSet Trainingsdaten
     * @param confidenceThreshold Zuverlässigkeits-Schwellenwert
     */
    public static TestRunResult testNeuralNetwork(MultiLayerPerceptron neuralNetwork,
            Map<String, DataSet> dataSets,
            double confidenceThreshold) {
        LOG.info("Starte Testlauf...");
        final TestRunResult testRunResult = new TestRunResult();

        for (final String testFile : dataSets.keySet()) {
            LOG.debug("Starte Testsatz mit Datei {}", testFile);
            final TestSetResult testSetResult = new TestSetResult(testFile);

            for (final DataSetRow testRow : dataSets.get(testFile).getRows()) { // Tests
                // Netzwerk Input setzen
                neuralNetwork.setInput(testRow.getInput());
                // Netzwerk berechnen
                neuralNetwork.calculate();

                final double[] nnOutput = neuralNetwork.getOutput().clone();
                desiredOutputManager.setOutput(testRow.getDesiredOutput());

                nnOutputManager.setOutput(nnOutput);
                testSetResult.addTestResult(new TestResult(
                        desiredOutputManager.getBestGuess().getKey(),
                        nnOutputManager.getBestGuess(confidenceThreshold).getKey(),
                        nnOutput)
                        );
            }
            testRunResult.addTestSetResult(testSetResult);
        }
        return testRunResult;
    }

}
