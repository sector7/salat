package de.fhw.salat.listener;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.core.events.LearningEventType;
import org.neuroph.nnet.learning.MomentumBackpropagation;

import de.fhw.salat.util.TableWriter;

/**
 * Klasse zum Export und zur Auswertung der dynamischen Parameter des Neuronalen Netzes.
 */
public abstract class AbstractDataExporter implements LearningEventListener {

    /** Datenstruktur zur Speicherung der Daten in eine Datei. */
    protected final TableWriter writer;
    /** Dateiname der auszugebenden CSV-Datei */
    protected final String fileName;

    /**
     * Konstruktor zur Erstellung eines Daten-Exporteurs.
     * 
     * @param fileName Dateiname der auszugebenden CSV-Datei
     * @param header Überschriften der CVS-Datei
     */
    public AbstractDataExporter(String fileName, String... header) {
        this(fileName, Arrays.asList(header));
    }

    /**
     * Konstruktor zur Erstellung eines Daten-Exporteurs.
     * 
     * @param fileName Dateiname der auszugebenden CSV-Datei
     * @param header Überschriften der CVS-Datei
     */
    public AbstractDataExporter(String fileName, List<String> header) {
        Objects.requireNonNull(fileName);
        this.fileName = fileName;
        this.writer = new TableWriter(header);
    }

    @Override
    public void handleLearningEvent(LearningEvent event) {
        if (event.getEventType() == LearningEventType.EPOCH_ENDED) {
            final MomentumBackpropagation mbp = (MomentumBackpropagation) event.getSource();
            learning(mbp);
        } else {
            learningStopped();
        }
    }

    /**
     * Methode zur Behandlung des Events, dass der Lernvorgang beended ist.
     */
    public void learningStopped() {
        this.writer.writeFile(this.fileName);
    }

    /**
     * Methode zur Behandlung einer Lerniteration.
     */
    public abstract void learning(final MomentumBackpropagation mbp);

}
