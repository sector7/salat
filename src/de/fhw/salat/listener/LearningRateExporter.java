package de.fhw.salat.listener;

import org.neuroph.nnet.learning.MomentumBackpropagation;

import de.fhw.salat.util.Plotter;
import de.fhw.salat.util.Plotter.PlotterType;

/**
 * Klasse zum Export und zur Auswertung der Lernrate des KNNs.
 */
public class LearningRateExporter extends AbstractDataExporter {

    /**
     * Konstruktor für den Exporteur.
     *
     * @param fileName Dateiname der auszugebenden CSV-Datei
     */
    public LearningRateExporter(String fileName) {
        super(fileName, "Iteration", "Error");
    }

    @Override
    public void learningStopped() {
        this.writer.writeFile(this.fileName);
        Plotter.createGraph(this.fileName, "doc/out/current/errorRate.png",
                PlotterType.LINE, "Veränderung der Fehlerrate", "Iterationen",
                "Fehlerrate");
    }

    @Override
    public void learning(final MomentumBackpropagation mbp) {
        this.writer.addRow((mbp.getCurrentIteration().toString()),
                Double.toString(mbp.getTotalNetworkError()));
    }

}
