package de.fhw.salat.listener;

import static de.fhw.salat.params.DefaultParams.defaultDynamicLearningRate;
import static de.fhw.salat.params.DefaultParams.defaultDynamicMomentum;

import java.util.ArrayList;
import java.util.List;

import org.neuroph.nnet.learning.MomentumBackpropagation;

import de.fhw.salat.params.ParamType;

/**
 * Klasse zum Export und zur Auswertung der dynamischen Parameter des Neuronalen Netzes.
 */
public class DynamicDataExporter extends AbstractDataExporter {

    /** Überschriften der CSV */
    private static List<String> header = new ArrayList<>();
    static {
        header.add("Iterationen");
        header.add("Fehlerrate");
        if (defaultDynamicLearningRate) {
            header.add(ParamType.LEARNING_RATE.description);
        }
        if (defaultDynamicMomentum) {
            header.add(ParamType.MOMENTUM.description);
        }
    }

    /**
     * Konstruktor für den Exporteur.
     *
     * @param fileName Dateiname der auszugebenden CSV-Datei
     */
    public DynamicDataExporter(String fileName) {
        super(fileName, header);
    }

    @Override
    public void learning(MomentumBackpropagation mbp) {
        final List<String> row = new ArrayList<>();
        row.add(mbp.getCurrentIteration().toString());
        row.add(Double.toString(mbp.getTotalNetworkError()));
        if (defaultDynamicLearningRate) {
            row.add(Double.toString(mbp.getLearningRate()));
        }
        if (defaultDynamicMomentum) {
            row.add(Double.toString(mbp.getMomentum()));
        }
        this.writer.addRow(row);
    }

}
