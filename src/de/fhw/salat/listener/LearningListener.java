package de.fhw.salat.listener;

import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.core.events.LearningEventType;
import org.neuroph.nnet.learning.MomentumBackpropagation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhw.salat.params.DefaultParams;

/**
 * Listener zur Behandlung der Lern-Events.
 */
public class LearningListener implements LearningEventListener {

    /** Logger */
    private final static Logger LOG = LoggerFactory.getLogger(LearningListener.class);

    /* (non-Javadoc)
     * @see org.neuroph.core.events.LearningEventListener#handleLearningEvent(org.neuroph.core.events.LearningEvent)
     */
    @Override
    public void handleLearningEvent(LearningEvent event) {
        final MomentumBackpropagation neuralNetwork = (MomentumBackpropagation) event.getSource();
        if (neuralNetwork.getCurrentIteration() % 100 == 0 // Nur alle 100 Schritte, um die Konsole
                                                           // nicht zu überfordern
                || neuralNetwork.getCurrentIteration() == 1
                || event.getEventType() == LearningEventType.LEARNING_STOPPED) {
            LOG.debug("Current iteration: {}", neuralNetwork.getCurrentIteration());
            LOG.debug("Error: {}", neuralNetwork.getTotalNetworkError());
            if (DefaultParams.defaultDynamicLearningRate) {
                LOG.debug("Dyn. Lernrate: {}", neuralNetwork.getLearningRate());
            }
            if (DefaultParams.defaultDynamicMomentum) {
                LOG.debug("Dyn. Momentum: {}", neuralNetwork.getMomentum());
            }
            if (event.getEventType() == LearningEventType.LEARNING_STOPPED) {
                LOG.info("Lernen nach {} Iterationen gestoppt. (Fehlerrate: {})",
                        neuralNetwork.getCurrentIteration(), neuralNetwork.getTotalNetworkError());
            }
        }
    }

}
