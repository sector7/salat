package de.fhw.salat.input;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;

import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Klasse zum Einlesen einer Patternfile (*.pat) in dem in der Aufgabenstellung spezifizierten
 * Format.
 */
final class PatternFileParser {

    /** Relativer Pfad zu den Pattern-Dateien */
    private static final String PATTERN_PATH = "resource/pattern/";

    /** Dateiendung der Patternfiles */
    private static final String PATTERN_EXT = ".pat";

    /** Logger */
    private final static Logger LOG = LoggerFactory.getLogger("input.PatternFileParser");

    /** File-Handle auf die spezifizierte Patternfile. */
    private final File file;

    /** Anzahl der Zeilen der Eingabematrix. */
    private int rowsIn;
    /** Anzahl der Spalten der Eingabematrix. */
    private int colsIn;
    /** Anzahl der Zeilen der Ausgabematrix. */
    private int rowsOut;
    /** Anzahl der Spalte der Ausgabematrix. */
    private int colsOut;

    /**
     * Neuer Parser für Patternfiles.
     *
     * @param patternFile Dateiname der zu ladenden Patternfile
     */
    protected PatternFileParser(String patternFile) {
        this.file = new File(PATTERN_PATH + patternFile + PATTERN_EXT);
    }

    /**
     * Liest die Patterndaten aus der Datei.
     * 
     * @return Patterndaten in Neuroph-kompatiblem Format
     */
    public DataSet getTrainingData() {
        LOG.debug("Lese Patternfile \"{}\"", this.file.getAbsolutePath());
        DataSet data = null;
        // Einlesen der Patternfile
        try (final LineNumberReader reader = new LineNumberReader(new FileReader(this.file))) {
            DataSetRow row;
            data = parseHeader(reader);
            // Alle Datensätze einlesen und jeweils an Trainingsdatensatz anhängen
            while ((row = parseData(reader)) != null) {
                data.addRow(row);
            }
            LOG.debug("Patternfile {} mit {} Einträgen erfolgreich gelesen", this.file.getPath(),
                    data.size());
        } catch (final FileNotFoundException e) {
            LOG.error("Datei nicht gefunden: ", e);
        } catch (final IOException e) {
            LOG.error("Fehler beim Lesen der Datei: ", e);
        } catch (final ParseException pe) {
            LOG.error("Ungültige Patternfile: ", pe);
        }
        return data;
    }

    /**
     * Liest einen Trainingsdatensatz ein.
     *
     * @param reader Datei-Handler
     * @return Einzelner Trainingsdatensatz oder <tt>null</tt>, wenn keine Datensätze mehr folgen
     * @throws ParseException wenn der Datensatz ein ungültiges Format aufweist
     * @throws IOException wenn ein IO-Fehler auftritt
     */
    private DataSetRow parseData(LineNumberReader reader) throws IOException, ParseException {
        final double[] input = parseMatrix(reader, this.rowsIn, this.colsIn);
        if (input == null)
            // Dateiende erreicht
            return null;
        final double[] output = parseMatrix(reader, this.rowsOut, this.colsOut);
        return new DataSetRow(input, output);
    }

    /**
     * Liest eine Matrix der angegebenen Größe aus der Patternfile.
     *
     * @param reader Datei-Handler
     * @param rows Erwartete Zeilenanzahl
     * @param cols Erwartete Spaltenanzahl
     * @return eingelesene Matrix oder <tt>null</tt>, wenn Ende der Datei erreicht ist
     * @throws ParseException wenn der Datensatz ein ungültiges Format aufweist
     * @throws IOException wenn ein IO-Fehler auftritt
     */
    private double[] parseMatrix(LineNumberReader reader, int rows, int cols) throws IOException,
    ParseException {
        final double[] matrix = new double[rows * cols];
        for (int row = 0; row < rows; row++) {
            final String rowString = reader.readLine(); // Zeilenweise vollständig einlesen
            if (rowString == null)
                if (row == 0) // Einzige Stelle, an der wir das Dateiende erwarten
                    return null;
                else
                    throw new ParseException(String.format(
                            "Matrix hat zu wenig Zeilen (erwartet %d, aber %d)", rows,
                            row), reader.getLineNumber());
            // Zeile an Leerzeichen trennen
            final String[] rowStrings = rowString.trim().split("\\s+");
            if (rowStrings.length != cols)
                throw new ParseException(String.format(
                        "Matrix hat falsche Spaltenanzahl (erwartet %d, aber %d)",
                        cols, rowStrings.length), reader.getLineNumber());
            for (int col = 0; col < cols; col++) {
                try {
                    // Eigentliches Füllen der Matrix
                    matrix[row * cols + col] = Double.parseDouble(rowStrings[col]);
                } catch (final NumberFormatException nfe) {
                    throw new ParseException("Ungültige Zahl: " + nfe.getLocalizedMessage(),
                            reader.getLineNumber());
                }
            }
        }
        return matrix;
    }

    /**
     * Liest die Spezifikation der Patternfile ein.
     *
     * @param reader Datei-Handler
     * @return Vorbereitete Trainingsdatentabelle
     * @throws ParseException wenn der Datensatz ein ungültiges Format aufweist
     * @throws IOException wenn ein IO-Fehler auftritt
     *
     */
    private DataSet parseHeader(LineNumberReader reader) throws IOException, ParseException {
        try {
            // Zeilenweise spezifizierte Matrixgrößen einlesen
            this.rowsIn = Integer.parseInt(reader.readLine());
            this.colsIn = Integer.parseInt(reader.readLine());
            this.rowsOut = Integer.parseInt(reader.readLine());
            this.colsOut = Integer.parseInt(reader.readLine());
            return new DataSet(this.rowsIn * this.colsIn, this.rowsOut * this.colsOut);
        } catch (final NumberFormatException nfe) {
            throw new ParseException("Ungültige Zahl: " + nfe, reader.getLineNumber());
        }
    }
}
