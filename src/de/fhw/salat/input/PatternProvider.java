package de.fhw.salat.input;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;

/**
 * Klasse zur Bereitstellung der Musterdateien für das Training und das Testen von Künstlichen
 * Neuronalen Netzwerken.
 */
public final class PatternProvider {
    /** Cache für bereits eingelesene Dateien */
    private static Map<Set<String>, DataSet> _cache = new HashMap<>();

    /**
     * Liefert den Datensatz mehrerer Dateien.
     *
     * @param fileNames Sammlung der Dateien
     * @return einzelner Datensatz mit sämtlichen Mustern
     */
    public static final DataSet get(Collection<String> fileNames) {
        final HashSet<String> distinctFilenames = new HashSet<>(fileNames);
        if (fileNames != null && !_cache.containsKey(distinctFilenames)) {
            _cache.put(distinctFilenames, actualGet(distinctFilenames));
        }
        return _cache.get(distinctFilenames);
    }

    /**
     * Liefert die Datensätze mehrerer Dateien aufgeschlüsselt nach Dateinamen
     *
     * @param fileNames Sammlung der einzulesenden Dateien
     * @return Datensätze aufgeschlüsselt nach Dateinamen
     */
    public static final Map<String, DataSet> getByName(Collection<String> fileNames) {
        final Map<String, DataSet> byName = new HashMap<>();
        // Alle Dateien einzeln einlesen
        for (final String trainingFile : fileNames) {
            byName.put(trainingFile, get(trainingFile));
        }
        return byName;
    }

    /**
     * Liefert den Datensatz einer einzelnen Patterndatei.
     *
     * @param fileName einzulesende Patterndatei
     * @return Datensatz der Patterndatei
     */
    public static final DataSet get(String fileName) {
        return get(Collections.singleton(fileName));
    }

    /**
     * Liest eine Menge von Dateien ein, wenn sie noch nicht im Cache vorhanden sind.
     *
     * @param fileNames einzulesende Dateien
     * @return Datensatz mit allen Mustern der spezifizierten Dateien
     */
    private static final DataSet actualGet(Set<String> fileNames) {
        DataSet result = null;
        for (final String fileName : fileNames) {
            final Set<String> singletonFile = Collections.singleton(fileName);
            DataSet fileContent;
            if (_cache.containsKey(singletonFile)) {
                fileContent = _cache.get(singletonFile);
            } else {
                fileContent = new PatternFileParser(fileName).getTrainingData();
                _cache.put(singletonFile, fileContent);
            }

            if (result == null) {
                result = new DataSet(fileContent.getInputSize(), fileContent.getOutputSize());
            }
            for (final DataSetRow row : fileContent.getRows()) {
                result.addRow(row);
            }
        }
        return result;
    }
}
