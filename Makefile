# --------------------------------------------------- #
# Fachhochschule Wedel, Sommersemester 2015           #
# Learning und Softcomputing - Makefile               #
# --- Gruppe 4 ---                                    #
#    - Ellen Schwartau, 101493, inf101493@fh-wedel.de #
#    - Malte Jörgens, 101519, inf101519@fh-wedel.de   #
#    - Timo Gröger, 101504, inf101504@fh-wedel.de     #
# --------------------------------------------------- #

.PHONY: all clean cleanall javadoc build run test

default: build

build:
	ant build

run:
	ant -Darg=$(arg) run

clean:
	ant clean

cleanall:
	ant cleanall

javadoc:
	ant javadoc

test:
	ant test

all:
	ant all